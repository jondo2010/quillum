# Quillum Simulator

[![pipeline status](https://gitlab.com/jondo2010/quillum/badges/master/pipeline.svg)](https://gitlab.com/jondo2010/quillum/commits/master)

[![coverage report](https://gitlab.com/jondo2010/quillum/badges/master/coverage.svg)](https://gitlab.com/jondo2010/quillum/commits/master)

## Getting Started

### Requirements

Quillum uses [Bazel](http://bazel.build).

It currently builds with clang-3.8; (clang-3.9 and OSX-Clang should also work)

### Build the Simulator

```
$ bazel build :quil
```

### Run the Simulator

```
$ bazel run :quil -- --help
Quillum Simulator 0.0.1

Generic Options:
  -v [ --version ]                      Print version string
  -h [ --help ]                         Produce help message
  -L [ --fmu-path ] arg (=.)            FMU search path, colon-separated
  --net arg (="")                       Network configuration file to use

Logging Options:
  -l [ --log-file ] arg (="")           Output logging file
  --min-log-level arg (=error)
                                        Minimum logging level:
                                        [trace,debug,info,warn,error,fatal,none,]
  --fmi-logging [=arg(=1)] (=0)         Enable logging from the FMI core

Debug Options:
  --debug-network [=arg(=1)] (=0)       Print debug information about each
                                        import in the network to stdout.
  --graph-network [=arg(=1)] (=0)       Write initial network data to a
                                        Graphviz file and quit

Simulation Options:
  -o [ --trace-file ] arg (="")         Output trace file. Defaults to
                                        'net_name.sdf'
  --end-time arg (=2)                   Simulaton end time.
  --step-size arg (=0.10000000000000001)
                                        Maximum step size
  --tol arg (=9.9999999999999995e-07)   Error tolerance
```

## Top-level Directory Structure

Folder name | Description
----------- | ----------
src         | Quillum source code
tests       | Quillum-specific unit tests

## Running the tests

Hopefully coming soon!

## Deployment

Hopefully coming soon!

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Authors

* **John Hughes**
