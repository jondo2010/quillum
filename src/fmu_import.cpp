/**
 *  @file   import_import.cpp
 *  @brief
 *  @since  Nov 16, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#include "fmu_import.hpp"

#include <array>
#include <cstdlib>
#include <memory>

#include <boost/exception/errinfo_api_function.hpp>
#include <boost/exception/errinfo_errno.hpp>
#include <boost/exception/errinfo_type_info_name.hpp>
#include <boost/format.hpp>
#include <boost/range/algorithm/find.hpp>

#include <fmilib.h>

#include "fmu_instance.hpp"
#include "logging.hpp"

namespace std
{
template <>
struct default_delete<fmi2_import_variable_list_t>
{
    void operator()(fmi2_import_variable_list_t *vl) { fmi2_import_free_variable_list(vl); }
};
template <>
struct default_delete<fmi_import_context_t>
{
    void operator()(fmi_import_context_t *c) { fmi_import_free_context(c); }
};
template <>
struct default_delete<fmi2_import_t>
{
    void operator()(fmi2_import_t *fmu) { fmi2_import_free(fmu); }
};
} /* namespace std */

namespace quillum
{
/**
 * @brief   Simple function that throws an exception with a message if the passed pointer is null.
 * Suitable for use in initializer lists.
 */
template <typename T>
T *null_check(T *p, const char *m)
{
    if (!p)
    {
        throw std::invalid_argument(m);
    }
    return p;
}

namespace fmi
{
/**
 * @brief   log_mapping maps the FMILibrary severity level to our Severity enum values.
 */
const std::array<Severity, jm_log_level_all + 1> log_mapping{
    {[jm_log_level_nothing] = Severity::none, [jm_log_level_fatal] = Severity::fatal,
     [jm_log_level_error] = Severity::error, [jm_log_level_warning] = Severity::warning,
     [jm_log_level_info] = Severity::info, [jm_log_level_verbose] = Severity::trace,
     [jm_log_level_debug] = Severity::debug, [jm_log_level_all] = Severity::debug}};

static void import_logger(const jm_callbacks *c, const jm_string module,
                          const jm_log_level_enu_t log_level, const jm_string message)
{
    FmuImport *const import = static_cast<FmuImport *const>(c->context);
    BOOST_LOG_SCOPED_LOGGER_TAG(import->get_log(), "Module", std::string(module));
    BOOST_LOG_SEV(import->get_log(), log_mapping[log_level]) << message;
}

/**
 * @brief   Implementation of FmuImport
 *
 * @note    We pass a pointer to this in the jm_callbacks_.context member so that import_logger()
 *          can get access.
 */
FmuImport::FmuImport(std::string const &import_path, const bool enable_fmi_logging)
  : log_source_(std::make_unique<LoggerType>(boost::log::keywords::channel = "Import"))
  , jm_callbacks_{std::malloc,
                  std::calloc,
                  std::realloc,
                  std::free,
                  import_logger,
                  enable_fmi_logging ? jm_log_level_all : jm_log_level_nothing,
                  static_cast<jm_voidp>(this)}
  , context_(
        (null_check(fmi_import_allocate_context(&jm_callbacks_), "Failed to allocate context.")),
        fmi_import_free_context)
  , temp_dir_()
  , version_(
        fmi_import_get_fmi_version(context_.get(), import_path.c_str(), temp_dir_.path.c_str()))
  , import_(null_check(fmi2_import_parse_xml(context_.get(), temp_dir_.path.c_str(), 0),
                       "Failed to parse XML"),
            fmi2_import_free)
  , fmi2_callbacks_{fmi2_log_forwarding, std::calloc, std::free, NULL, import_.get()}
  , model_name_(fmi2_import_get_model_name(import_.get()))
{
    BOOST_LOG_FUNCTION();
    BOOST_LOG_SEV(*log_source_, Severity::info)
        << boost::format("Loaded [%1%] from %2%.") % get_model_name() % import_path;

    if (context_ == NULL)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal) << "Error allocating context in FmuImport()";
        throw FmuImportException() << boost::errinfo_api_function("FmuImport::FmuImport()")
                                   << boost::errinfo_type_info_name("Error allocating context");
    }

    if (version_ != fmi_version_2_0_enu)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal) << "fmi_import_get_fmi_version() "
                                                        "reports unsupported version. "
                                                        "Does the file exist?";
        throw FmuImportException()
            << boost::errinfo_api_function("FmuImport::FmuImport()")
            << boost::errinfo_type_info_name(
                   "fmi_import_get_fmi_version() reports unsupported version. Does the "
                   "file exist?");
    }

    if (!fmi2_import_get_capability(import_.get(), fmi2_me_providesDirectionalDerivatives))
    {
        BOOST_LOG_SEV(*log_source_, Severity::warning)
            << "FMU does not support calculation of directional derivatives!";
    }

    jm_status_enu_t jmstatus;
    jmstatus = fmi2_import_create_dllfmu(import_.get(), fmi2_fmu_kind_me, &fmi2_callbacks_);
    if (jmstatus == jm_status_error)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "Could not create the DLL loading mechanism(C-API):"
            << fmi2_import_get_last_error(import_.get())
            << " Probably wasn't able to find the model library for this platform.";
        throw FmuImportException() << boost::errinfo_api_function("FmuImport::FmuImport()")
                                   << fmi_error_string(fmi2_import_get_last_error(import_.get()));
    }

    number_of_continuous_states_ = (fmi2_import_get_number_of_continuous_states(import_.get()));
    number_of_event_indicators_ = (fmi2_import_get_number_of_event_indicators(import_.get()));
}

FmuImport::~FmuImport() { BOOST_LOG_SEV(*log_source_, Severity::trace) << "~FmuImport()"; }
std::vector<std::string> FmuImport::get_source_files() const
{
    std::vector<std::string> sources;
    size_t num_files = fmi2_import_get_source_files_cs_num(import_.get());
    for (uint32_t i = 0U; i < num_files; i++)
    {
        sources.push_back(fmi2_import_get_source_file_cs(import_.get(), i));
    }
    return sources;
}

std::string FmuImport::get_shlib_path() const
{
    std::unique_ptr<const char[]> dll_path(fmi_import_get_dll_path(
        temp_dir_.path.c_str(), fmi2_import_get_model_identifier_CS(import_.get()),
        &jm_callbacks_));
    return std::string(dll_path.get());
}

std::string FmuImport::get_temp_path() const { return temp_dir_.path.string(); }
FmuImport::VarType FmuImport::get_variable_by_name(std::string const &name) const
{
    fmi2_import_variable_t *const var =
        fmi2_import_get_variable_by_name(import_.get(), name.c_str());
    if (!var)
    {
        BOOST_LOG_NAMED_SCOPE("get_variable_by_name()");
        boost::format error =
            boost::format("FMU variable [%1%] not found in import [%2%].") % name % model_name_;
        BOOST_LOG_SEV(logger::get(), Severity::fatal) << error;
        throw std::runtime_error(error.str());
    }
    return VarType(var, import_);
}

FmuImport::VarRangeType FmuImport::get_input_variable_range() const
{
    const std::unique_ptr<fmi2_import_variable_list_t> vl(
        fmi2_import_get_variable_list(import_.get(), 0));
    const fmi2_causality_enu_t c = fmi2_causality_enu_input;
    const std::shared_ptr<fmi2_import_variable_list_t> vl_filt(
        fmi2_import_filter_variables(vl.get(), &causality_filter, (void *)(&c)),
        std::default_delete<fmi2_import_variable_list_t>());
    return VarRangeType(vl_filt, import_);
}

FmuImport::VarRangeType FmuImport::get_output_variable_range() const
{
    const std::shared_ptr<fmi2_import_variable_list_t> vl(
        fmi2_import_get_outputs_list(import_.get()),
        std::default_delete<fmi2_import_variable_list_t>());
    return VarRangeType(vl, import_);
}

FmuImport::VarRangeType FmuImport::get_constant_range() const
{
    const std::unique_ptr<fmi2_import_variable_list_t> vl(
        fmi2_import_get_variable_list(import_.get(), 0));
    const std::shared_ptr<fmi2_import_variable_list_t> vl_filt(
        fmi2_import_filter_variables(vl.get(), constant_filter, NULL),
        std::default_delete<fmi2_import_variable_list_t>());
    return VarRangeType(vl_filt, import_);
}

FmuImport::VarRangeType FmuImport::get_parameter_range() const
{
    const std::unique_ptr<fmi2_import_variable_list_t> vl(
        fmi2_import_get_variable_list(import_.get(), 0));
    const fmi2_causality_enu_t c = fmi2_causality_enu_parameter;
    const std::shared_ptr<fmi2_import_variable_list_t> vl_filt(
        fmi2_import_filter_variables(vl.get(), &causality_filter, (void *)(&c)),
        std::default_delete<fmi2_import_variable_list_t>());
    return VarRangeType(vl_filt, import_);
}

FmuImport::VarRangeType FmuImport::get_derivative_range() const
{
    const std::shared_ptr<fmi2_import_variable_list_t> vl(
        fmi2_import_get_derivatives_list(import_.get()),
        std::default_delete<fmi2_import_variable_list_t>());
    return VarRangeType(vl, import_);
}

FmuImport::VarRangeType FmuImport::get_output_dependency_range(VarType const &output) const
{
    if (output.get_details().causality != fmi2_causality_enu_output)
    {
        // Since this isn't an output variable, and can't have any dependencies, return an
        // empty range
        return VarRangeType();
    }

    // Find out the index of this Variable in the output list
    const VarRangeType output_range = get_output_variable_range();
    const VarIterType output_it = boost::range::find(output_range, output);
    size_t output_index = std::distance(output_range.begin(), output_it);

    // Get the dependency data
    size_t *start_index, *dependency;
    char *factor_kind;

    fmi2_import_get_outputs_dependencies(import_.get(), &start_index, &dependency, &factor_kind);
    return get_range_from_dependency_arrays(start_index, dependency, factor_kind, output_index);
}

FmuImport::VarRangeType FmuImport::get_derivative_dependency_range(VarType const &derivative) const
{
    if (derivative.get_details().causality != fmi2_causality_enu_local &&
        derivative.get_details().variability != fmi2_variability_enu_continuous)
    {
        return VarRangeType();
    }

    // Find out the index of this Variable in the derivatives list
    const VarRangeType derivative_range = get_derivative_range();
    const VarIterType derivative_it = boost::range::find(derivative_range, derivative);
    size_t derivative_index = std::distance(derivative_range.begin(), derivative_it);

    // Get the dependency data
    size_t *start_index, *dependency;
    char *factor_kind;

    fmi2_import_get_derivatives_dependencies(import_.get(), &start_index, &dependency,
                                             &factor_kind);
    return get_range_from_dependency_arrays(start_index, dependency, factor_kind, derivative_index);
}

FmuImport::VarRangeType FmuImport::get_derivative_dependency_range() const
{
    // Get the dependency data
    size_t *start_index;
    size_t *dependency;
    char *factor_kind;
    fmi2_import_get_derivatives_dependencies(import_.get(), &start_index, &dependency,
                                             &factor_kind);

    /**
     * The output dependencies are in the dependency[] array range
     * (start_index[derivative_index] -> start_index[derivative_index+1]]
     * NULL pointer is returned if no dependency information was provided in the XML.
     */
    if (start_index)
    {
        // Now create a new variable list from the dependency data and the output_index
        std::shared_ptr<fmi2_import_variable_list_t> const dependency_vl(
            fmi2_import_alloc_variable_list(import_.get(), 0),
            std::default_delete<fmi2_import_variable_list_t>());
        std::unique_ptr<fmi2_import_variable_list_t> const all_variables_vl(
            fmi2_import_get_variable_list(import_.get(), 0));

        for (size_t derivative_index = 0; derivative_index < number_of_continuous_states_;
             derivative_index++)
        {
            /*
            const size_t num_dependencies = start_index[derivative_index + 1] -
            start_index[derivative_index];
            BOOST_LOG_SEV(log_source_, Severity::trace) << "Found " << num_dependencies << "
            dependencies";
            */

            for (size_t i = start_index[derivative_index]; i < start_index[derivative_index + 1];
                 i++)
            {
                fmi2_import_variable_t *const v =
                    fmi2_import_get_variable(all_variables_vl.get(), dependency[i] - 1);
                fmi2_import_var_list_push_back(dependency_vl.get(), v);
            }
        }
        return VarRangeType(dependency_vl, import_);
    }
    else
    {
        /**
         * If not present, it must be assumed that the Unknown depends on all Knowns.
         * If present as empty list, the Unknown depends on none of the Knowns.
         * Otherwise the Unknown depends on the Knowns defined by the given ScalarVariable indices.
         */
        return get_input_variable_range();
    }
}

FmuImport::VarRangeType FmuImport::get_initial_unknowns_range() const
{
    const std::shared_ptr<fmi2_import_variable_list_t> vl(
        fmi2_import_get_initial_unknowns_list(import_.get()),
        std::default_delete<fmi2_import_variable_list_t>());
    return VarRangeType(vl, import_);
}

FmuImport::VarRangeType FmuImport::get_initial_unknowns_dependency_range(
    FmuImport::VarType const &unknown) const
{
    // Find out the index of this Variable in the initial unknowns list
    const VarRangeType unknown_range = get_initial_unknowns_range();
    const VarIterType unknown_it = boost::range::find(unknown_range, unknown);
    size_t unknown_index = std::distance(unknown_range.begin(), unknown_it);

    // Get the dependency data
    size_t *start_index;
    size_t *dependency;
    char *factor_kind;
    fmi2_import_get_initial_unknowns_dependencies(import_.get(), &start_index, &dependency,
                                                  &factor_kind);
    return get_range_from_dependency_arrays(start_index, dependency, factor_kind, unknown_index);
}

const char *FmuImport::get_last_error() const { return fmi2_import_get_last_error(import_.get()); }
void FmuImport::print_structure() const
{
    std::stringstream ss;
    ss << "Dumping structure for FmuImport [" << get_model_name() << "]:\n";
    ss << "  Description: " << get_description() << "\n";
    ss << "  Number of continuous states: " << get_number_of_continuous_states() << "\n";
    ss << "  Number of event indicators: " << get_number_of_event_indicators() << "\n";
    ss << "  Input Ports:\n";

    for (VarType const &v : get_input_variable_range())
    {
        ss << "    " << v << " \"" << v.get_description() << "\"\n";
    }

    ss << "  Output Ports:\n";
    for (VarType const &output : get_output_variable_range())
    {
        ss << "    " << output;
        ss << "<-(";
        for (VarType const &output_dep : get_output_dependency_range(output))
        {
            ss << output_dep.get_name() << ", ";
        }
        ss << ") \"" << output.get_description() << "\"\n";
    }

    ss << "  Continuous Derivatives:\n";
    for (VarType const &der : get_derivative_range())
    {
        ss << "    " << der;
        ss << "<-(";
        for (VarType const &der_dep : get_derivative_dependency_range(der))
        {
            ss << der_dep.get_name() << ", ";
        }
        ss << ") \"" << der.get_description() << "\"\n";
    }

    /*
    ss << "\tParameters:\n";
    for (VarType const &param : get_parameter_range())
    {
        ss << "\t\t" << param << " \"" << param.get_description() << "\"\n";
    }
    */

    ss << "  Initial Unknowns:\n";
    for (VarType const &unknown : get_initial_unknowns_range())
    {
        ss << "    " << unknown;
        ss << "<-(";
        for (VarType const &unknown_dep : get_initial_unknowns_dependency_range(unknown))
        {
            ss << unknown_dep.get_name() << ", ";
        }
        ss << ") \"" << unknown.get_description() << "\"\n";
    }

    std::cout << ss.str() << std::endl;
}

FmuImport::VarRangeType FmuImport::get_range_from_dependency_arrays(
    size_t *const &start_index, size_t *const &dependency, char *const &factor_kind,
    const size_t variable_index) const
{
    (void)factor_kind;

    /**
     * The output dependencies are in the dependency[] array range
     * (startIndex[variable_index] -> startIndex[variable_index+1]]
     * NULL pointer is returned if no dependency information was provided in the XML.
     */
    if (start_index)
    {
        // const size_t num_dependencies = startIndex[variable_index + 1] -
        // startIndex[variable_index];
        // Now create a new variable list from the dependency data and the output_index
        std::shared_ptr<fmi2_import_variable_list_t> const dependency_vl(
            fmi2_import_alloc_variable_list(import_.get(), 0),
            std::default_delete<fmi2_import_variable_list_t>());
        std::unique_ptr<fmi2_import_variable_list_t> const all_variables_vl(
            fmi2_import_get_variable_list(import_.get(), 0));

        for (size_t i = start_index[variable_index]; i < start_index[variable_index + 1]; i++)
        {
            if (dependency[i] == 0)
            {
                // Index equals to zero means "depends on all" (no information in the XML).
                return get_input_variable_range();
            }
            else
            {
                fmi2_import_variable_t *const v =
                    fmi2_import_get_variable(all_variables_vl.get(), dependency[i] - 1);
                fmi2_import_var_list_push_back(dependency_vl.get(), v);
            }
        }
        return VarRangeType(dependency_vl, import_);
    }
    else
    {
        return VarRangeType();
    }
}

std::ostream &operator<<(std::ostream &stream, FmuImport const &import)
{
    return stream << import.get_model_name();
}

} /* namespace fmi */
} /* namespace quillum */
