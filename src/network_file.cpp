/**
 *  @file   network_file.cpp
 *  @brief  Network configuration file handling
 *  @since  June 27, 2017
 *  @author John Hughes <jondo2010@gmail.com>
 */

#include "network_file.hpp"

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include "logging.hpp"

namespace quillum
{
namespace boom
{
namespace network
{
/**
 * @brief   Helper function to cast and convert a Property Tree value_type to a VariantType
 * @param   value   The ptree::value_type to cast and convert
 * @param   type    The fmi2 type to convert.
 */
template <class VariantType>
static VariantType convert_ptree_to_variant(boost::property_tree::ptree::value_type const& value,
                                            const fmi2_base_type_enu_t type)
{
    VariantType var;
    switch (type)
    {
    case fmi2_base_type_real:
        var = value.second.get_value<double>();
        break;
    case fmi2_base_type_int:
        var = value.second.get_value<int>();
        break;
    case fmi2_base_type_bool:
        var = value.second.get_value<bool>();
        break;
    case fmi2_base_type_str:
        var = value.second.get_value<char*>();
    case fmi2_base_type_enum:
    default:
        break;
    }
    return var;
}

template <class InstanceT>
void load_config_file(Network<InstanceT>& net, boost::filesystem::path const& network_file,
                      ProgramConfig const& config,
                      ImportFactory<typename InstanceT::ImportType> const& import_factory,
                      InstanceFactory<InstanceT> const& instance_factory)
{
    using InstanceType = InstanceT;
    using ImportType = typename InstanceT::ImportType;
    using VarMap = typename InstanceT::VarMap;
    using VarType = typename InstanceT::VarType;
    using VariantType = typename InstanceT::VariantType;

    if (!network_file.empty())
    {
        boost::property_tree::ptree tree;
        boost::property_tree::read_json(network_file.string(), tree);

        // Add all Imports
        for (boost::property_tree::ptree::value_type const& v : tree.get_child("network.imports"))
        {
            const std::string url = v.second.get<std::string>("url");
            const bool logging = v.second.get<bool>("enable_fmi_logging");

            boost::filesystem::path fmu_file = find_file_in_path(url, config.fmu_paths);
            if (!boost::filesystem::is_regular_file(fmu_file))
            {
                BOOST_LOG_SEV(net.log_source, Severity::fatal) << "Unable to find import file "
                                                               << fmu_file << " looking in path \""
                                                               << config.fmu_paths << "\"";
                throw boost::filesystem::filesystem_error(
                    "Network::Network()", fmu_file,
                    boost::system::errc::make_error_code(
                        boost::system::errc::no_such_file_or_directory));
            }
            import_factory(fmu_file.string(), logging);
        }

        // Add all Instances of Imports
        for (boost::property_tree::ptree::value_type const& v : tree.get_child("network.instances"))
        {
            const std::string import_name = v.second.get<std::string>("import_name");
            try
            {
                // Find the Import
                typename ImportType::Ptr& import = net.imports.at(import_name);

                using SelfType = boost::property_tree::ptree::value_type::second_type;

                // Find the initial parameters
                VarMap initial_parameters;
                if (boost::optional<SelfType const&> param =
                        v.second.get_child_optional("parameters"))
                {
                    for (boost::property_tree::ptree::value_type const& p : *param)
                    {
                        VarType var = import->get_variable_by_name(p.first);
                        const fmi2_base_type_enu_t type = var.get_details().type;
                        initial_parameters.insert(
                            std::make_pair(var, convert_ptree_to_variant<VariantType>(p, type)));
                    }
                }

                // Find the initial conditions in the Import
                VarMap initial_conditions;
                if (boost::optional<SelfType const&> cond =
                        v.second.get_child_optional("initial_conditions"))
                {
                    for (boost::property_tree::ptree::value_type const& c : *cond)
                    {
                        VarType var = import->get_variable_by_name(c.first);
                        const fmi2_base_type_enu_t type = var.get_details().type;
                        initial_conditions.insert(
                            std::make_pair(var, convert_ptree_to_variant<VariantType>(c, type)));
                    }
                }

                const std::string instance_name = v.first;
                const bool visible = v.second.get<bool>("visible", false);
                const bool debug_logging = v.second.get<bool>("debug_logging", false);
                const double start_time = 0;

                typename InstanceType::Ptr instance = instance_factory(
                    import, instance_name, visible, initial_conditions, initial_parameters);
                initialize_instance(net, instance, start_time, debug_logging, config);
            }
            catch (std::out_of_range const& e)
            {
                boost::format error =
                    boost::format("FMU import [%1%] not found in imports list.") % import_name;
                BOOST_LOG_SEV(net.log_source, Severity::fatal) << error;
                throw std::runtime_error(error.str());
            }
        }

        // Add all connections between Instances
        for (boost::property_tree::ptree::value_type const& v :
             tree.get_child("network.connections"))
        {
            add_connection(net, v.second.get<std::string>("from_instance"),
                           v.second.get<std::string>("from_port"),
                           v.second.get<std::string>("to_instance"),
                           v.second.get<std::string>("to_port"));
        }
    }
}

// Explicit Instantiations.
using Import = quillum::fmi::FmuImport;
using Variant = quillum::fmi::FmiVariant;
using Instance = quillum::fmi::FmuInstance<Import, Variant>;
template void load_config_file<Instance>(Network<Instance>& net,
                                         boost::filesystem::path const& network_file,
                                         ProgramConfig const& config, ImportFactory<Import> const&,
                                         InstanceFactory<Instance> const&);

} /* namespace network */
} /* namespace boom */
} /* namespace quillum */
