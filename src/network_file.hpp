/**
 *  @file   network_file.hpp
 *  @brief  Network configuration file handling
 *  @since  June 27, 2017
 *  @author John Hughes <jondo2010@gmail.com>
 */
#pragma once

#include <boost/filesystem/path.hpp>

#include "network.hpp"

namespace quillum
{
namespace boom
{
namespace network
{
/**
 * @brief   Load a network configuration file
 * @param[in,out]   net
 * @param[in]       network_file
 * @param[in]       config
 * @param[in]       import_factory
 * @param[in]       instance_factory
 */
template <class InstanceT>
void load_config_file(Network<InstanceT>& net, boost::filesystem::path const& network_file,
                      ProgramConfig const& config,
                      ImportFactory<typename InstanceT::ImportType> const& import_factory,
                      InstanceFactory<InstanceT> const& instance_factory);

} /* namespace network */
} /* namespace boom */
} /* namespace quillum */
