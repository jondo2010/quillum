/**
 *  @file   network.cpp
 *  @brief
 *  @since  Nov 27, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#include "network.hpp"

#include <boost/algorithm/string/join.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/format.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/property_map/transform_value_property_map.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>

#include "logging.hpp"
#include "network_graphviz.hpp"
#include "utility.hpp"

namespace quillum
{
namespace boom
{
namespace network
{
template <class InstanceT>
Network<InstanceT>::Network()
  : num_states(0)
  , num_event_indicators(0)
  , log_source(boost::log::keywords::channel = "Network")
{
    // Add a Vertex for the tear node
    /*
    {
        GraphVertex vertex = boost::add_vertex(graph_);
        const VertexTypePMap vertex_type_map = boost::get(vertex_type_t(), graph_);
        vertex_type_map[vertex] = VertexType::TEAR;
    }
    */
}

template <class NetworkT>
typename NetworkT::ImportType::Ptr import_factory(NetworkT& net, std::string const& fmu_path,
                                                  const bool enable_fmi_logging)
{
    typename NetworkT::ImportType::Ptr import =
        std::make_shared<typename NetworkT::ImportType>(fmu_path, enable_fmi_logging);
    typename NetworkT::ImportMap::const_iterator it = net.imports.find(import->get_model_name());

    if (it == net.imports.end())
    {
        it = net.imports.emplace_hint(it, std::make_pair(import->get_model_name(), import));
    }
    else
    {
        BOOST_LOG_SEV(net.log_source, Severity::warning)
            << boost::format("FMU import [%1%] already found in the network while importing %2%") %
                   import->get_model_name() % fmu_path;
    }
    return it->second;
}

template <class NetworkT>
typename NetworkT::InstanceType::Ptr instance_factory(
    NetworkT& net, typename NetworkT::ImportType::Ptr import, std::string const& name,
    const bool visible, typename NetworkT::InstanceType::VarMap const& initial_conditions,
    typename NetworkT::InstanceType::VarMap const& params)
{
    typename NetworkT::InstanceMap::const_iterator it = net.instances.find(name);

    if (it == net.instances.end())
    {
        it = net.instances.emplace_hint(
            it, std::make_pair(name, std::make_shared<typename NetworkT::InstanceType>(
                                         import, name, visible, initial_conditions, params)));
    }
    else
    {
        BOOST_LOG_SEV(net.log_source, Severity::warning)
            << boost::format("FMU instance [%1%] already exists in the network") % name;
    }
    return it->second;
}

template <class NetworkT>
typename NetworkT::FilteredGraph get_init_graph(NetworkT const& net)
{
    return
        typename NetworkT::FilteredGraph(net.graph,
                                         [&](typename NetworkT::GraphEdge const& e) -> bool {
                                             return (net.graph[e].type != EdgeType::INTERNAL_CTS);
                                         },
                                         [&](typename NetworkT::GraphVertex const& v) -> bool {
                                             return (net.graph[v].type != VertexType::CALCPARAM);
                                         });
}

template <class NetworkT>
typename NetworkT::FilteredGraph get_norm_graph(NetworkT const& net)
{
    return
        typename NetworkT::FilteredGraph(net.graph,
                                         [&](typename NetworkT::GraphEdge const& e) -> bool {
                                             return (net.graph[e].type != EdgeType::INTERNAL_INIT);
                                         },
                                         [&](typename NetworkT::GraphVertex const& v) -> bool {
                                             return ((net.graph[v].type != VertexType::CALCPARAM) &&
                                                     (net.graph[v].type != VertexType::PARAM));
                                         });
}

template <class NetworkT>
void add_connection(NetworkT& net, std::string const& from_instance_name,
                    std::string const& from_variable, std::string const& to_instance_name,
                    std::string const& to_variable)
{
    // Property maps for the SubGraph (Important to remember these map local vertices)
    typename NetworkT::EdgeBundleMap edge_bundle = boost::get(boost::edge_bundle, net.graph);

    std::string conn_string =
        boost::str(boost::format("[%1%.%2%] -> [%3%.%4%]") % from_instance_name % from_variable %
                   to_instance_name % to_variable);

    BOOST_LOG_SEV(net.log_source, Severity::info) << "Adding requested connection between "
                                                  << conn_string;

    typename NetworkT::GraphVertex from =
        find_vertex_in_graph(net, from_instance_name, from_variable, VertexType::OUTPORT);
    typename NetworkT::GraphVertex to =
        find_vertex_in_graph(net, to_instance_name, to_variable, VertexType::INPORT);

    std::pair<typename NetworkT::GraphEdge, bool> edge_pair = boost::add_edge(from, to, net.graph);
    edge_bundle[edge_pair.first].type = EdgeType::CONNECTION;
}

template <class NetworkT>
void initialize_instance(NetworkT& net, typename NetworkT::InstanceType::Ptr const& instance,
                         const double start_time, const bool debug_logging,
                         ProgramConfig const& config)
{
    using SubGraph = typename NetworkT::SubGraph;
    using VertexBundleMap = typename NetworkT::VertexBundleMap;
    using EdgeBundleMap = typename NetworkT::EdgeBundleMap;
    using VarType = typename NetworkT::VarType;
    using GraphVertexPredicate = typename NetworkT::GraphVertexPredicate;
    using GraphVertex = typename NetworkT::GraphVertex;
    using GraphEdge = typename NetworkT::GraphEdge;

    instance->set_debug_logging(debug_logging);
    instance->setup_experiment(config.tolerance, start_time, -1.0);

    net.num_states += instance->get_import().get_number_of_continuous_states();
    net.num_event_indicators += instance->get_import().get_number_of_event_indicators();

    // Create a subgraph for this instance
    SubGraph& subgraph = net.graph.create_subgraph();
    boost::get_property(subgraph, boost::graph_bundle).instance = instance;

    // Property maps for the SubGraph (Important to remember these map local vertices)
    VertexBundleMap vertex_bundle = boost::get(boost::vertex_bundle, subgraph);
    EdgeBundleMap edge_bundle = boost::get(boost::edge_bundle, subgraph);

    // Filters
    GraphVertexPredicate inport_pred = [&](GraphVertex const& v) -> bool {
        return vertex_bundle[v].type == VertexType::INPORT;
    };
    boost::filtered_graph<SubGraph, boost::keep_all, GraphVertexPredicate> in_vertex_subgraph(
        subgraph, boost::keep_all(), inport_pred);

    GraphVertexPredicate outport_pred = [&](GraphVertex const& v) -> bool {
        return vertex_bundle[v].type == VertexType::OUTPORT;
    };
    boost::filtered_graph<SubGraph, boost::keep_all, GraphVertexPredicate> out_vertex_subgraph(
        subgraph, boost::keep_all(), outport_pred);

    // Add all input ports
    for (VarType const& in_port : instance->get_import().get_input_variable_range())
    {
        GraphVertex in_vertex = add_vertex_for_variable(net, subgraph, in_port);
        (void)in_vertex;
    }

    // Add all states and state derivatives
    for (VarType const& der : instance->get_import().get_derivative_range())
    {
        GraphVertex der_vertex = add_vertex_for_variable(net, subgraph, der);

        VarType state = der.get_state_for_derivative();
        GraphVertex state_vertex = add_vertex_for_variable(net, subgraph, state);
        vertex_bundle[state_vertex].type = VertexType::STATE;

        // Add a dependency edge for every variable that the derivative depends on
        for (VarType const& der_dep : instance->get_import().get_derivative_dependency_range(der))
        {
            /*
               BOOST_LOG_SEV(net.log_source, Severity::debug) << "Derivative [" << der
               << "] depends on [" << der_dep << "].";
               */

            GraphVertex dep_vertex = add_vertex_for_variable(net, subgraph, der_dep);
            std::pair<GraphEdge, bool> edge_pair =
                boost::add_edge(dep_vertex, der_vertex, subgraph);
            edge_bundle[edge_pair.first].type = EdgeType::INTERNAL_CTS;
        }
    }

    // Add all output ports
    for (VarType const& out_port : instance->get_import().get_output_variable_range())
    {
        GraphVertex out_vertex = add_vertex_for_variable(net, subgraph, out_port);

        // Add a dependency edge for every input variable that this output variable depends on
        for (VarType const& dependent_variable :
             instance->get_import().get_output_dependency_range(out_port))
        {
            /*
               BOOST_LOG_SEV(net.log_source, Severity::debug)
               << "Output [" << out_port << "] depends on [" << dependent_variable << "].";
               */

            GraphVertex dep_vertex = add_vertex_for_variable(net, subgraph, dependent_variable);
            std::pair<GraphEdge, bool> edge_pair =
                boost::add_edge(dep_vertex, out_vertex, subgraph);
            edge_bundle[edge_pair.first].type = EdgeType::INTERNAL_CTS;
        }
    }

    // Process initial unknowns and dependencies
    for (VarType const& initial_unknown : instance->get_import().get_initial_unknowns_range())
    {
        /*
           BOOST_LOG_SEV(log_source_, Severity::debug) << "Processing initial unknown ["
           << initial_unknown << "]";
           */
        GraphVertex unknown_vertex = add_vertex_for_variable(net, subgraph, initial_unknown);

        for (VarType const& dependent_variable :
             instance->get_import().get_initial_unknowns_dependency_range(initial_unknown))
        {
            /*
               BOOST_LOG_SEV(log_source_, Severity::debug) << "Initial unknown ["
               << initial_unknown << "] depends on ["
               << dependent_variable << "].";
               */

            GraphVertex dependent_vertex =
                add_vertex_for_variable(net, subgraph, dependent_variable);
            std::pair<GraphEdge, bool> edge_pair =
                boost::add_edge(dependent_vertex, unknown_vertex, subgraph);
            if (edge_pair.second)
            {
                edge_bundle[edge_pair.first].type = EdgeType::INTERNAL_INIT;
            }
            else
            {
                // The edge already exists
                edge_bundle[edge_pair.first].type = EdgeType::INTERNAL_BOTH;
            }
        }
    }
}

template <class NetworkT>
typename NetworkT::GraphVertex find_vertex_in_graph(NetworkT const& net,
                                                    std::string const& instance_name,
                                                    std::string const& variable_name,
                                                    const VertexType vertex_type)
{
    using InstanceMap = typename NetworkT::InstanceMap;
    using VarType = typename NetworkT::VarType;
    using SubGraph = typename NetworkT::SubGraph;
    using VertexBundleMap = typename NetworkT::VertexBundleMap;
    using GraphVertex = typename NetworkT::GraphVertex;
    using VertexBundleMap = typename NetworkT::VertexBundleMap;

    // Find the instance in the instance map
    typename InstanceMap::const_iterator instance_it = net.instances.find(instance_name);
    if (instance_it == net.instances.end())
    {
        throw std::invalid_argument("Network::find_vertex_in_graph(): [" + instance_name +
                                    "] not found.");
    }

    const VarType variable = instance_it->second->get_import().get_variable_by_name(variable_name);

    // Find the subgraph corresponding to the instance
    using SubGraphRangeType = boost::iterator_range<typename SubGraph::children_iterator>;
    SubGraphRangeType subgraph_range = boost::make_iterator_range(net.graph.children());

    typename SubGraph::children_iterator subgraph_it =
        boost::range::find_if(subgraph_range, [&](SubGraph const& g) -> bool {
            return instance_it->second == boost::get_property(g, boost::graph_bundle).instance;
        });

    if (subgraph_it == subgraph_range.end())
    {
        throw std::invalid_argument(
            "Network::find_vertex_in_graph(): Matching subgraph not found.");
    }

    // Property map for the SubGraph (Important to remember these map local vertices)
    VertexBundleMap vertex_bundle = boost::get(boost::vertex_bundle, *subgraph_it);

    // Find the vertex corresponding to the variable
    using VertexRangeType = boost::iterator_range<typename SubGraph::vertex_iterator>;
    VertexRangeType vertex_range = boost::make_iterator_range(boost::vertices(*subgraph_it));

    typename SubGraph::vertex_iterator vertex_it =
        boost::range::find_if(vertex_range, [&](GraphVertex const& v) -> bool {
            std::shared_ptr<VarType> var = vertex_bundle[v].var;
            assert(var);
            return (variable == *var);
        });

    if (vertex_it == vertex_range.end())
    {
        throw std::invalid_argument("Network::find_vertex_in_graph(): Matching vertex not found.");
    }

    assert(vertex_bundle[*vertex_it].type == vertex_type);

    // Convert to a global vertex
    return subgraph_it->local_to_global(*vertex_it);
}

template <class NetworkT>
typename NetworkT::GraphVertex add_vertex_for_variable(NetworkT& net,
                                                       typename NetworkT::SubGraph& graph,
                                                       typename NetworkT::VarType const& var)
{
    using InstanceType = typename NetworkT::InstanceType;
    using VertexBundleMap = typename NetworkT::VertexBundleMap;
    using GraphVertex = typename NetworkT::GraphVertex;
    using VarType = typename NetworkT::VarType;

    // Property map for the SubGraph (Important to remember these map local vertices)
    VertexBundleMap vertex_bundle = boost::get(boost::vertex_bundle, graph);

    for (GraphVertex vertex : boost::make_iterator_range(boost::vertices(graph)))
    {
        if (var == *vertex_bundle[vertex].var.get())
        {
            return vertex;
        }
    }

    // Not found, so add
    GraphVertex vertex = boost::add_vertex(graph);
    vertex_bundle[vertex].var = std::make_shared<VarType>(var);

    typename InstanceType::Ptr instance =
        boost::get_property(graph, boost::graph_bundle).instance;

    boost::format log_formatter("Adding {%1%} [%2%] to [%3%] as global vertex [%4%]");

    switch (var.get_details().causality)
    {
    case fmi2_causality_enu_parameter:
        vertex_bundle[vertex].type = VertexType::PARAM;
        log_formatter % "param";
        break;
    case fmi2_causality_enu_calculated_parameter:
        vertex_bundle[vertex].type = VertexType::CALCPARAM;
        log_formatter % "calc param";
        break;
    case fmi2_causality_enu_input:
        vertex_bundle[vertex].type = VertexType::INPORT;
        log_formatter % "input";
        break;
    case fmi2_causality_enu_output:
        vertex_bundle[vertex].type = VertexType::OUTPORT;
        log_formatter % "output";
        break;
    case fmi2_causality_enu_local:
        vertex_bundle[vertex].type = VertexType::DER;
        log_formatter % "der";
        break;
    case fmi2_causality_enu_independent:
    case fmi2_causality_enu_unknown:
    default:
        BOOST_LOG_SEV(net.log_source, Severity::warning) << "Unhandled Variable causality [" << var
                                                         << "]";
        break;
    }

    log_formatter % var % instance->get_name() % graph.local_to_global(vertex);
    BOOST_LOG_SEV(net.log_source, Severity::trace) << log_formatter;
    return vertex;
}

template <class NetworkT>
void tear_loops(NetworkT& net)
{
    using SubGraph = typename NetworkT::SubGraph;
    using GraphEdge = typename NetworkT::GraphEdge;
    using GraphVertex = typename NetworkT::GraphVertex;
    using GraphEdgePredicate = typename NetworkT::GraphEdgePredicate;
    using VertexBundleMap = typename NetworkT::VertexBundleMap;
    using VarType = typename NetworkT::VarType;
    using VertexBundle = typename NetworkT::VertexBundle;

    // Start by resetting the flow (tear) property on all edges in the graph
    for (GraphEdge const& edge : boost::make_iterator_range(boost::edges(net.graph)))
    {
        net.graph[edge].flow = false;
    }

    // Create a filtered view of the graph omitting torn edges
    GraphEdgePredicate torn_pred = [&](GraphEdge const& e) -> bool {
        return net.graph[e].flow == false;
    };
    boost::filtered_graph<SubGraph, GraphEdgePredicate, boost::keep_all> torn_graph(
        net.graph, torn_pred, boost::keep_all());

    VertexBundleMap vertex_bundle = boost::get(boost::vertex_bundle, net.graph);

    auto name_map = boost::make_transform_value_property_map(
        boost::bind(&VarType::get_name, _1), boost::get(&VertexBundle::var, net.graph));

    // Main tearing loop, upper bound the max iterations by the number of edges.
    for (size_t i = 0; i < boost::num_edges(net.graph); i++)
    {
        // Identify strongly connected components (SCCs) in the graph
        std::vector<std::unordered_set<GraphVertex>> nontrivial_components =
            identify_connected_components(torn_graph);

        if (nontrivial_components.size() < 1)
        {
            break;  // Break out of the tearing loop
        }

        BOOST_LOG_SEV(net.log_source, Severity::debug)
            << "Identified " << boost::size(nontrivial_components)
            << " non-trivial Strongly-Connected-Component(s)";

        for (std::unordered_set<GraphVertex> const& comp : nontrivial_components)
        {
            BOOST_LOG_SEV(net.log_source, Severity::debug)
                << "Component ["
                << boost::algorithm::join(
                       comp | boost::adaptors::transformed(
                                  [&](GraphVertex v) -> std::string { return name_map[v]; }),
                       ", ")
                << "]";

            // Iterate through all vertices in this SCC and give them an asymmetry score
            using VertexScore = std::pair<GraphVertex, double>;
            std::vector<VertexScore> scores(comp.size());
            for (auto v : comp | boost::adaptors::indexed())
            {
                const size_t d_in = boost::in_degree(v.value(), net.graph);
                const size_t d_out = boost::out_degree(v.value(), net.graph);
                scores[v.index()] = std::make_pair(v.value(), std::max(d_in / d_out, d_out / d_in));
            }

            VertexScore max_element = *boost::range::max_element(
                scores, [](VertexScore const& l, VertexScore const& r) -> bool {
                    return l.second < r.second;
                });

            BOOST_LOG_SEV(net.log_source, Severity::debug)
                << boost::format("Tearing variable [%1%].") % *vertex_bundle[max_element.first].var;

            // Mark all in or out edges as torn, whichever set has smaller cardinality
            if (boost::in_degree(max_element.first, net.graph) <
                boost::out_degree(max_element.first, net.graph))
            {
                for (GraphEdge edge :
                     boost::make_iterator_range(boost::in_edges(max_element.first, net.graph)))
                {
                    net.graph[edge].flow = true;
                }
            }
            else
            {
                for (GraphEdge edge :
                     boost::make_iterator_range(boost::out_edges(max_element.first, net.graph)))
                {
                    net.graph[edge].flow = true;
                }
            }

            /*
            std::cout << "Scores ["
                      << boost::algorithm::join(
                             scores | boost::adaptors::transformed(
                                          static_cast<std::string (*)(double)>(std::to_string)),
                             ", ")
                      << "]" << std::endl;
            ;
            */
        }
    }
}

template <class NetworkT>
void enter_initialization_mode(NetworkT const& net)
{
    using InstanceMap = typename NetworkT::InstanceMap;
    for (typename InstanceMap::const_reference instance_pair : net.instances)
    {
        instance_pair.second->enter_initialization();
    }
}

template <class NetworkT>
void exit_initialization_mode(NetworkT const& net)
{
    using InstanceMap = typename NetworkT::InstanceMap;
    for (typename InstanceMap::const_reference instance_pair : net.instances)
    {
        instance_pair.second->exit_initialization();
    }
}

template <class NetworkT>
void enter_continuous_time_mode(NetworkT const& net)
{
    using InstanceMap = typename NetworkT::InstanceMap;
    for (typename InstanceMap::const_reference instance_pair : net.instances)
    {
        instance_pair.second->enter_continuous_time_mode();
    }
}

template <class NetworkT>
void write_graphviz(NetworkT const& net, ProgramConfig const& config)
{
    GraphvizBuilder<NetworkT> builder(net.graph, "network");
    boost::filesystem::path gv_path = config.network_file.filename();
    gv_path.replace_extension("dot");
    builder.write_to_file(gv_path);
}

template <class NetworkT>
void print_debug_information(NetworkT const& net)
{
    using ImportMap = typename NetworkT::ImportMap;
    for (typename ImportMap::const_reference import_pair : net.imports)
    {
        import_pair.second->print_structure();
    }

    std::cout << "Number of network states: " << net.num_states << std::endl;
    std::cout << "Number of event indicators: " << net.num_event_indicators << std::endl;
}

// Explicit Instantiations.
using Import = quillum::fmi::FmuImport;
using Variant = quillum::fmi::FmiVariant;
using Instance = quillum::fmi::FmuInstance<Import, Variant>;
using Net = Network<Instance>;
using FilteredGraph = typename Network<Instance>::FilteredGraph;
template Network<Instance>::Network();

template void enter_initialization_mode<Net>(Net const&);
template void exit_initialization_mode<Net>(Net const&);
template void enter_continuous_time_mode<Net>(Net const&);
template void write_graphviz<Net>(Net const&, ProgramConfig const&);
template void print_debug_information<Net>(Net const&);
template Net::FilteredGraph get_init_graph<Net>(Net const&);
template Net::FilteredGraph get_norm_graph<Net>(Net const&);
template void add_connection<Net>(Net& net, std::string const& from_instance_name,
                                  std::string const& from_variable,
                                  std::string const& to_instance_name,
                                  std::string const& to_variable);
template void initialize_instance<Net>(Net& net, typename Net::InstanceType::Ptr const& instance,
                                       const double start_time, const bool debug_logging,
                                       ProgramConfig const& config);

template void tear_loops<Net>(Net& net);

} /* namespace network */
} /* namespace boom */
} /* namespace quillum */
