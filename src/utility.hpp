/**
 *  @file   utility.cpp
 *  @brief  Genric utilities
 *  @since  March 21, 2017
 *  @author John Hughes <jondo2010@gmail.com>
 */
#pragma once

#include <string>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

namespace quillum
{
/**
 * @brief   Return a canonical path to a filename located in one of a list of directory paths.
 * @param   filename    The filename to search for.
 * @param   paths       A vector of filesystem::paths to search for filename in
 * @return              The canonicalized full-path to the filename if found with in paths,
 *                      otherwise returns filename.
 */
boost::filesystem::path find_file_in_path(std::string const& filename,
                                          std::vector<boost::filesystem::path> const& paths);

/**
 * @brief   Creates a temporary directory on construction, and deletes it upon destruction.
 */
struct TempDir
{
    TempDir()
      : path(boost::filesystem::temp_directory_path() / boost::filesystem::unique_path())
    {
        boost::filesystem::create_directory(path);
    }
    ~TempDir() { boost::filesystem::remove_all(path); }
    boost::filesystem::path path;
};

} /* namespace quillum */
