/**
 *  @file   utility.cpp
 *  @brief  Genric utilities
 *  @since  March 21, 2017
 *  @author John Hughes <jondo2010@gmail.com>
 */
#include "utility.hpp"

#include <boost/filesystem/operations.hpp>

#include "logging.hpp"

namespace quillum
{
boost::filesystem::path find_file_in_path(std::string const& filename,
                                          std::vector<boost::filesystem::path> const& paths)
{
    for (boost::filesystem::path const& path : paths)
    {
        if (boost::filesystem::is_directory(path) && boost::filesystem::exists(path))
        {
            boost::filesystem::path fmu_path = path / filename;
            fmu_path = boost::filesystem::system_complete(fmu_path);
            if (boost::filesystem::exists(fmu_path))
            {
                return boost::filesystem::canonical(fmu_path);
            }
        }
        else
        {
            BOOST_LOG_SEV(logger::get(), Severity::warning) << "Directory [" << path
                                                            << "] in fmu_path is invalid";
        }
    }
    return boost::filesystem::path(filename);
}
} /* namespace quillum */
