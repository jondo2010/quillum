/**
 *  @file   network.hpp
 *  @brief
 *  @since  Nov 27, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#pragma once

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/subgraph.hpp>
#include <boost/range/adaptor/indexed.hpp>

#include "config.hpp"
#include "fmu_import_variable.hpp"
#include "fmu_instance.hpp"

namespace quillum
{
namespace boom
{
namespace network
{
struct NetworkException : virtual boost::exception
{
};
struct NetworkInstanceNotFoundException : virtual NetworkException
{
};
struct NetworkInstanceVariableNotFoundException : virtual NetworkException
{
};

template <class ImportT>
using ImportFactory =
    std::function<typename ImportT::Ptr(std::string const&, const bool)>;

template <class InstanceT>
using InstanceFactory = std::function<typename InstanceT::Ptr(
    typename InstanceT::ImportType::Ptr const&, std::string const&, const bool,
    typename InstanceT::VarMap const&, typename InstanceT::VarMap const&)>;

enum class VertexType : size_t
{
    INPORT,    /*!< Vertex represents an input port. */
    OUTPORT,   /*!< Vertex represents an output port. */
    DER,       /*!< Vertex represents a state derivative. */
    STATE,     /*!< Vertex represents a state variable. */
    PARAM,     /*!< Vertex represents a parameter. */
    CALCPARAM, /*!< Vertex represents a calculated parameter. */
    TEAR,      /*!< Vertex represents the tear node. */
    Count
};

enum class EdgeType : size_t
{
    INTERNAL_INIT, /*!< Edge represents a dependency only during init. */
    INTERNAL_CTS,  /*!< Edge represents a dependency only during continuous mode. */
    INTERNAL_BOTH, /*!< Edge represents a dependency during init and continuous modes. */
    CONNECTION,    /*!< Edge represents a connection between FMUs. */
    TEAR,          /*!< Edge represents a connection through the tear node. */
    Count
};

/**
 * @brief   Identify Strongly Connected Components (SCCs) in the graph.
 * @param   graph   The graph to search for SCCs
 * @tparam  GraphT  The graph type.
 * @return  A vector of sets of vertices corresponding to the components.
 */
template <typename GraphT>
static inline std::vector<
    std::unordered_set<typename boost::graph_traits<GraphT>::vertex_descriptor>>
identify_connected_components(GraphT const& graph)
{
    using Vertex = typename boost::graph_traits<GraphT>::vertex_descriptor;

    const size_t num_vertices = boost::num_vertices(graph);
    std::vector<int> components(num_vertices);
    std::vector<int> discover_times(num_vertices);
    std::vector<boost::default_color_type> colors(num_vertices);
    std::vector<Vertex> roots(num_vertices);

    typename boost::property_map<GraphT, boost::vertex_index_t>::const_type vertex_index_map =
        boost::get(boost::vertex_index, graph);

    const int num_components = boost::strong_components(
        graph, boost::make_iterator_property_map(components.begin(), vertex_index_map),
        boost::root_map(boost::make_iterator_property_map(roots.begin(), vertex_index_map))
            .color_map(boost::make_iterator_property_map(colors.begin(), vertex_index_map))
            .discover_time_map(
                boost::make_iterator_property_map(discover_times.begin(), vertex_index_map)));

    std::vector<std::unordered_set<Vertex>> component_sets(num_components);
    for (auto c : components | boost::adaptors::indexed())
    {
        component_sets[(size_t)c.value()].insert(c.index());
    }

    // Remove trivial (smaller than 3 vertices) components from the component_sets vector
    component_sets.erase(
        std::remove_if(component_sets.begin(), component_sets.end(),
                       [](std::unordered_set<Vertex>& set) { return set.size() < 3; }),
        component_sets.end());

    return component_sets;
}

/**
 * @brief   Find the subgraph that a global vertex belongs to.
 * @param   graph   The parent subgraph to search.
 * @param   vertex  The child vertex to find.
 * @tparam  GraphT  The subgraph type.
 * @return  A reference to the subgraph containing vertex.
 */
template <typename GraphT>
static inline GraphT const& find_subgraph_for_vertex(
    GraphT& graph, const typename boost::graph_traits<GraphT>::vertex_descriptor vertex)
{
    for (GraphT& child : boost::make_iterator_range(graph.children()))
    {
        std::pair<typename boost::graph_traits<GraphT>::vertex_descriptor, bool> search =
            child.find_vertex(vertex);
        if (search.second)
        {
            return child;
        }
    }
    return graph;  // Not found, just return the parent?
}

/**
 * @brief   Network maintains the configuration of the network. This data is constant
 *          over a solver step. It changes by adding/removing instances and connections.
 * @tparam  InstanceT   The Instance class to use.
 */
template <class InstanceT>
struct Network
{
    using InstanceType = InstanceT;
    using ImportType = typename InstanceType::ImportType;
    using VariantType = typename InstanceType::VariantType;
    using VarType = typename ImportType::VarType;
    using ImportMap = std::unordered_map<std::string, typename ImportType::Ptr>;
    using InstanceMap = std::unordered_map<std::string, typename InstanceType::Ptr>;

    // Chain together the vertex properties
    struct VertexBundle
    {
        VertexType type;
        std::shared_ptr<VarType> var;
    };
    using VertexProperties = boost::property<boost::vertex_index_t, std::size_t, VertexBundle>;

    // Chain together the edge properties
    struct EdgeBundle
    {
        EdgeType type;
        bool flow;
    };
    using EdgeProperties = boost::property<boost::edge_index_t, std::size_t, EdgeBundle>;

    // Chain together the graph properties
    struct GraphBundle
    {
        typename InstanceType::Ptr instance;
    };
    using GraphProperties = GraphBundle;

    using Graph = boost::adjacency_list<
        boost::setS,           /* Use std::set for out edges. */
        boost::vecS,           /* Use std::vector for vertices. */
        boost::bidirectionalS, /* Store bidirectional connections between vertices. */
        VertexProperties,      /* Vertex properties list. */
        EdgeProperties,        /* Edge properties list. */
        GraphProperties        /* Graph properties list. */
        >;

    using SubGraph = boost::subgraph<Graph>;

    using GraphVertex = typename SubGraph::vertex_descriptor;
    using GraphEdge = typename SubGraph::edge_descriptor;

    using ConstVertexIdPMap =
        typename boost::property_map<SubGraph, boost::vertex_index_t>::const_type;
    using ConstEdgeIdPMap = typename boost::property_map<SubGraph, boost::edge_index_t>::const_type;

    using VertexBundleMap = typename boost::property_map<SubGraph, boost::vertex_bundle_t>::type;
    using VertexBundleMapConst =
        typename boost::property_map<SubGraph, boost::vertex_bundle_t>::const_type;

    using EdgeBundleMap = typename boost::property_map<SubGraph, boost::edge_bundle_t>::type;
    using EdgeBundleMapConst =
        typename boost::property_map<SubGraph, boost::edge_bundle_t>::const_type;

    using GraphVertexPredicate =
        std::function<bool(GraphVertex const&)>; /*!< Predicate for filtering vertices. */
    using GraphEdgePredicate =
        std::function<bool(GraphEdge const&)>; /*!< Predicate for filtering edges. */
    using FilteredGraph = boost::filtered_graph<SubGraph, GraphEdgePredicate, GraphVertexPredicate>;

    /**
     * @brief   Create a Network initialized with the program configuration
     */
    Network();

    ImportMap imports;           /*!< FMU Imports. */
    InstanceMap instances;       /*!< FMU Instances. */
    SubGraph graph;              /*!< The top-level network graph structure. */
    size_t num_states;           /*!< Total number of states in the network. */
    size_t num_event_indicators; /*!< Total number of event indicators in the network. */
    LoggerType log_source;       /*!< Logging source. */
};

/**
 * @brief   Add an FmuImport to the network
 * @param   fmu_path            The path to the FMU file.
 * @param   enable_fmi_logging  Enable logging from the FMI core.
 */
template <class NetworkT>
typename NetworkT::ImportType::Ptr import_factory(NetworkT& net, std::string const& fmu_path,
                                                  const bool enable_fmi_logging);

/**
 * @brief       Add an FmuInstance to the network.
 * @param[in]   import          The FmuImport to create an instance of
 * @param[in]   name            The unique name for this instance in the network
 * @param[in]   debug_logging   Whether to enable debug logging on this instance
 * @param[in]   visible         The visibility flag to pass to the instance
 * @param[in]   start_time      The initial simulation time to set in the instance
 */
template <class NetworkT>
typename NetworkT::InstanceType::Ptr instance_factory(
    NetworkT& net, typename NetworkT::ImportType::Ptr import, std::string const& name,
    const bool visible, typename NetworkT::InstanceType::VarMap const& initial_conditions,
    typename NetworkT::InstanceType::VarMap const& params);

/**
 * @brief   Return a const reference to the graph filtered for initialization.
 */
template <class NetworkT>
typename NetworkT::FilteredGraph get_init_graph(NetworkT const& net);

/**
 * @brief   Return a const reference to the graph filtered for normal execution.
 */
template <class NetworkT>
typename NetworkT::FilteredGraph get_norm_graph(NetworkT const& net);

/**
 * @brief   Add a connection to the network between and output and an input port.
 * @param   net                 A reference to the Network
 * @param   from_instance_name  The name of the instance containing the OUTPORT.
 * @param   from_variable       The name of the OUTPORT variable.
 * @param   to_instance_name    The name of the instance containing the INPORT.
 * @param   to_variable         The name of the INPORT variable.
 */
template <class NetworkT>
void add_connection(NetworkT& net, std::string const& from_instance_name,
                    std::string const& from_variable, std::string const& to_instance_name,
                    std::string const& to_variable);

/**
 * @brief           Initialize a new Instance in the Network
 * @param[in,out]   net             A reference to the Network
 * @param[in]       instance        Reference to the new Instance
 * @param[in]       start_time
 * @param[in]       debug_logging
 * @param[in]       config          Reference to the ProgramConfig.
 */
template <class NetworkT>
void initialize_instance(NetworkT& net, typename NetworkT::InstanceType::Ptr const& instance,
                         const double start_time, const bool debug_logging,
                         ProgramConfig const& config);

/**
 * @brief   Find a GraphVertex in the graph matching the instance and variable names.
 * @param   instance_name
 * @param   variable_name
 * @return  GraphVertex
 */
template <class NetworkT>
typename NetworkT::GraphVertex find_vertex_in_graph(NetworkT const& net,
                                                    std::string const& instance_name,
                                                    std::string const& variable_name,
                                                    const VertexType vertex_type);

/**
 * @brief    Add a Vertex to a graph given a Variable
 * @param    var     The Variable to add to the graph
 * @param    graph   The graph to search
 * @return   A reference to the added GraphVertex
 */
template <class NetworkT>
typename NetworkT::GraphVertex add_vertex_for_variable(NetworkT& net,
                                                       typename NetworkT::SubGraph& graph,
                                                       typename NetworkT::VarType const& var);

/**
 * @brief   Determine if there are any algebraic loops present in the network, and run a
 * heuristic to tag a minimum set of edges as 'tear' variables that will then be solved using a
 * non-linear step during integration.
 * @param   net     A reference to the Network
 */
template <class NetworkT>
void tear_loops(NetworkT& net);

/**
 * @brief   Enter every instance into initialization mode.
 */
template <class NetworkT>
void enter_initialization_mode(NetworkT const& net);

/**
 * @brief   Exit every instance from initialization mode. Implicitly enters event mode.
 */
template <class NetworkT>
void exit_initialization_mode(NetworkT const& net);

/**
 * @brief   Enter every instance into continuous time mode.
 */
template <class NetworkT>
void enter_continuous_time_mode(NetworkT const& net);

/**
 * @brief   Dump the current graph into a Graphviz file.
 */
template <class NetworkT>
void write_graphviz(NetworkT const& net, ProgramConfig const& config);

/**
 * @brief   Print debug information about the network.
 */
template <class NetworkT>
void print_debug_information(NetworkT const& net);

} /* namespace network */
} /* namespace boom */
} /* namespace quillum */
