/**
 *  @file   fmu_import_variable.cpp
 *  @brief
 *  @since  Nov 16, 2016
 *  @author johughes <jondo2010@gmail.com>
 */
#include "fmu_import_variable.hpp"

#include <boost/exception/errinfo_api_function.hpp>
#include <boost/exception/errinfo_errno.hpp>
#include <boost/exception/errinfo_type_info_name.hpp>
#include <boost/functional/hash.hpp>
#include <boost/smart_ptr/scoped_ptr.hpp>

#include "logging.hpp"

namespace quillum
{
namespace fmi
{
int causality_filter(const fmi2_import_variable_t* v, const void* data)
{
    return (int)(fmi2_import_get_causality(v) == *(fmi2_causality_enu_t*)data);
}

int constant_filter(const fmi2_import_variable_t* v, const void* data)
{
    (void)data;
    const fmi2_initial_enu_t initial = fmi2_import_get_initial(v);
    return (int)((initial == fmi2_initial_enu_exact) || (initial == fmi2_initial_enu_approx));
}

std::ostream& operator<<(std::ostream& stream, fmi2_base_type_enu_t const& type)
{
    switch (type)
    {
    case fmi2_base_type_real:
        stream << "R";
        break;
    case fmi2_base_type_int:
        stream << "I";
        break;
    case fmi2_base_type_bool:
        stream << "B";
        break;
    case fmi2_base_type_str:
        stream << "S";
        break;
    case fmi2_base_type_enum:
        stream << "E";
        break;
    default:
        stream << "Unknown";
    }
    return stream;
}

std::ostream& operator<<(std::ostream& stream, fmi2_causality_enu_t const& causality)
{
    switch (causality)
    {
    case fmi2_causality_enu_parameter:
        stream << "parameter";
        break;
    case fmi2_causality_enu_calculated_parameter:
        stream << "calculated_parameter";
        break;
    case fmi2_causality_enu_input:
        stream << "input";
        break;
    case fmi2_causality_enu_output:
        stream << "output";
        break;
    case fmi2_causality_enu_local:
        stream << "local";
        break;
    case fmi2_causality_enu_independent:
        stream << "independent";
        break;
    case fmi2_causality_enu_unknown:
    default:
        stream << "unknown";
        break;
    }
    return stream;
}

std::ostream& operator<<(std::ostream& stream, fmi2_variability_enu_t const& variability)
{
    switch (variability)
    {
    case fmi2_variability_enu_constant:
        stream << "constant";
        break;
    case fmi2_variability_enu_fixed:
        stream << "fixed";
        break;
    case fmi2_variability_enu_tunable:
        stream << "tunable";
        break;
    case fmi2_variability_enu_discrete:
        stream << "discrete";
        break;
    case fmi2_variability_enu_continuous:
        stream << "continuous";
        break;
    case fmi2_variability_enu_unknown:
        stream << "unknown";
        break;
    }
    return stream;
}

std::ostream& operator<<(std::ostream& stream, fmi2_initial_enu_t const& initial)
{
    switch (initial)
    {
    case fmi2_initial_enu_exact:
        stream << "exact";
        break;
    case fmi2_initial_enu_approx:
        stream << "approx";
        break;
    case fmi2_initial_enu_calculated:
        stream << "calculated";
        break;
    case fmi2_initial_enu_unknown:
        stream << "unknown";
        break;
    }
    return stream;
}

std::ostream& operator<<(std::ostream& stream, Variable const& variable)
{
    return stream << variable.get_name() << " <" << variable.get_details().type << '/'
                  << variable.get_details().causality << '>';
}

Variable::Details::Details(fmi2_import_variable_t* const v)
  : type(fmi2_import_get_variable_base_type(v))
  , causality(fmi2_import_get_causality(v))
  , variability(fmi2_import_get_variability(v))
  , initial(fmi2_import_get_initial(v))
{
    // Gather further type information
    switch (type)
    {
    case fmi2_base_type_real:
    {
        const fmi2_import_real_variable_t* real_v = fmi2_import_get_variable_as_real(v);

        if (real_v)
        {
            reinit = fmi2_import_get_real_variable_reinit(real_v);
            start = fmi2_import_get_real_variable_start(real_v);
            min = fmi2_import_get_real_variable_min(real_v);
            max = fmi2_import_get_real_variable_max(real_v);
            nominal = fmi2_import_get_real_variable_nominal(real_v);

            fmi2_import_unit_t* const unit_ = fmi2_import_get_real_variable_unit(real_v);
            if (unit_)
            {
                unit = fmi2_import_get_unit_name(unit_);
            }
            fmi2_import_display_unit_t* const display_unit_ =
                fmi2_import_get_real_variable_display_unit(real_v);
            if (display_unit_)
            {
                display_unit = fmi2_import_get_display_unit_name(display_unit_);
            }
        }
    }
    break;
    case fmi2_base_type_int:
    {
        fmi2_import_integer_variable_t* int_v = fmi2_import_get_variable_as_integer(v);
        if (int_v)
        {
            start = fmi2_import_get_integer_variable_start(int_v);
            min = fmi2_import_get_integer_variable_min(int_v);
            max = fmi2_import_get_integer_variable_max(int_v);
        }
    }
    break;
    case fmi2_base_type_bool:
    {
        fmi2_import_bool_variable_t* bool_v = fmi2_import_get_variable_as_boolean(v);
        if (bool_v)
        {
            start = fmi2_import_get_boolean_variable_start(bool_v);
        }
    }
    break;
    case fmi2_base_type_str:
    case fmi2_base_type_enum:
    default:
        BOOST_LOG_SEV(logger::get(), Severity::fatal) << "Variable details not handled for " << type
                                                      << ".";
    }
}

Variable::Variable(fmi2_import_variable_t* const v, const std::shared_ptr<fmi2_import_t> import)
  : v_(v)
  , import_(import)
  , vr_(fmi2_import_get_variable_vr(v_))
  , details_(v_)
{
    if (!v_)
    {
        throw std::invalid_argument("Invalid fmi2_import_variable_t in Variable::Variable()");
    }
}

const std::string Variable::get_name() const { return fmi2_import_get_variable_name(v_); }
const std::string Variable::get_description() const
{
    // Check for NULL pointer returned from fmi2_import_get_variable_description()
    const char* description = fmi2_import_get_variable_description(v_);
    if (description)
    {
        return std::string(description);
    }
    else
    {
        return std::string();
    }
}

Variable Variable::get_state_for_derivative() const
{
    fmi2_import_real_variable_t* const v_real = as_real();
    if (!v_real)
    {
        throw FmuImportException()
            << boost::errinfo_api_function("Variable::get_state_for_derivative()")
            << boost::errinfo_type_info_name("Error casting variable as real");
    }
    fmi2_import_real_variable_t* const v_der_real =
        fmi2_import_get_real_variable_derivative_of(v_real);
    if (!v_der_real)
    {
        throw FmuImportException()
            << boost::errinfo_api_function("Variable::get_state_for_derivative()")
            << boost::errinfo_type_info_name("No state variable found");
    }
    return Variable((fmi2_import_variable_t*)v_der_real, import_);
}

void Variable::dump_info() const
{
    fmi2_variability_enu_t variability = fmi2_import_get_variability(v_);
    std::cout << "Variability: " << variability;
    std::cout << std::endl;

    fmi2_causality_enu_t causality = fmi2_import_get_causality(v_);
    std::cout << "Causality: " << causality;
    std::cout << std::endl;

    fmi2_initial_enu_t initial = fmi2_import_get_initial(v_);
    std::cout << "Initial: " << initial;
    std::cout << std::endl;
}

bool Variable::operator==(Variable const& other) const { return v_ == other.v_; }
bool Variable::operator!=(Variable const& other) const { return v_ != other.v_; }
std::size_t hash_value(Variable const& variable)
{
    boost::hash<size_t> hasher;
    return hasher(((size_t)variable.v_) / sizeof(fmi2_import_variable_t*));
}

} /* namespace fmi */
} /* namespace quillum */
