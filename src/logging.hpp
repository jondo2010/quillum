/**
 *  @file   logging.hpp
 *  @brief  Definitions for logging
 *  @since  Feb 10, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */

#pragma once

#include <iostream>
#include <map>
#include <vector>

#include <boost/log/attributes/constant.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/attributes/scoped_attribute.hpp>
#include <boost/log/expressions/keyword.hpp>
#include <boost/log/sources/channel_feature.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>
#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/severity_feature.hpp>

namespace quillum
{
/**
 * Logging severity enumeration
 */
enum class Severity
{
    trace,    //!< trace
    debug,    //!< debug
    info,     //!< info
    warning,  //!< warning
    error,    //!< error
    fatal,    //!< fatal
    none      //!< none
};

typedef std::map<Severity, std::string> SeverityMap;

const SeverityMap severity_map{{Severity::trace, "trace"}, {Severity::debug, "debug"},
                               {Severity::info, "info"},   {Severity::warning, "warn"},
                               {Severity::error, "error"}, {Severity::fatal, "fatal"},
                               {Severity::none, "none"}};

/**
 * Severity keyword for logging
 */
BOOST_LOG_ATTRIBUTE_KEYWORD(SeverityKeyword, "Severity", Severity)

BOOST_LOG_ATTRIBUTE_KEYWORD(ModuleKeyword, "Module", std::string)

typedef boost::log::sources::severity_channel_logger_mt<Severity>
    LoggerType; /*!< Application logger type. */

BOOST_LOG_GLOBAL_LOGGER(logger, LoggerType)

/**
 * @brief   Initialize the logging backend
 * @param   level   The minimum severity level to log
 */
void init_logging(const Severity level);

/**
 * @brief   Return a stringified list of the severity keys
 */
const std::string severityKeyList();

/**
 * @brief   Overloaded istream operator to extract Severity enum
 * @param   in
 * @param   level
 * @return
 */
std::istream& operator>>(std::istream& in, Severity& level);

/**
 * @brief   Overloaded ostream operator to write a Severity enum to string
 * @param   strm
 * @param   level
 * @return
 */
std::ostream& operator<<(std::ostream& strm, const Severity level);

} /* namespace quillum */

namespace std
{
template <typename T>
inline std::ostream& operator<<(std::ostream& s, std::vector<T> const& v)
{
    s.put('[');
    char comma[3] = {'\0', ' ', '\0'};
    for (const auto& e : v)
    {
        s << comma << e;
        comma[0] = ',';
    }
    return s << ']';
}
}
