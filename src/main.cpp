/**
 *  @file   main.cpp
 *  @brief  Main entrypoint to the Quillum simulator
 *  @since  Nov 16, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */

#include <chrono>
#include <csignal>
#include <iostream>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include "config.hpp"

namespace
{
volatile std::sig_atomic_t signal_status;
}

extern "C" void signal_handler(int status)
{
    signal_status = status;
}

int main(int argc, char **argv)
{
    namespace fs = boost::filesystem;
    fs::path config_path;

    // prefix_path is the install directory
    fs::path prefix_path(fs::initial_path<fs::path>());
    prefix_path = fs::system_complete(fs::path(argv[0]).parent_path().parent_path());

    quillum::boom::ProgramConfig config;
    bool want_exit;
    std::tie(config, want_exit) = quillum::boom::parse_options(argc, argv);

    quillum::init_logging(config.severity);

    if (want_exit)
    {
        return 0;
    }

    //using Variant = quillum::fmi::FmiVariant;
    //using Import = quillum::fmi::FmuImport;
    //using Instance = quillum::fmi::FmuInstance<Import, Variant>;
    //using Network = quillum::boom::network::Network<Instance>;
    //Network net(config);

    if (config.graph_and_quit)
    {
        // Output GraphViz
        //net.write_graphviz();
    }

    if (config.dump_and_quit)
    {
        // Print debut information about the network
        //net.print_debug_information();
    }

    if (config.graph_and_quit || config.dump_and_quit)
    {
        return 0;
    }

    // Install the signal handler
    // TODO make this actually work..
    // std::signal(SIGINT, signal_handler);

    //quillum::boom::executor::execute(net);

    return 0;
}
