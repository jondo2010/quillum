/**
 *  @file   network_graphviz.hpp
 *  @brief  Transform the Network Graph into one suitable for outputting to GraphViz format.
 *  @since  March 29, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#pragma once

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/subgraph.hpp>

namespace quillum
{
namespace boom
{
namespace network
{
template <typename NetworkT>
class GraphvizBuilder
{
    // We use an adjacency_list specialization that has all the required Graphviz parameters
    using GraphvizAttributes = std::unordered_map<std::string, std::string>;
    using GraphvizVertexparameters = boost::property<boost::vertex_attribute_t, GraphvizAttributes>;

    using _EdgeAttribute = boost::property<boost::edge_attribute_t, GraphvizAttributes>;
    using GraphvizEdgeparameters = boost::property<boost::edge_index_t, size_t, _EdgeAttribute>;

    using _GraphEdgeAttribute = boost::property<boost::graph_edge_attribute_t, GraphvizAttributes>;
    using _GraphVertexAttribute =
        boost::property<boost::graph_vertex_attribute_t, GraphvizAttributes, _GraphEdgeAttribute>;
    using _GraphGraphAttribute =
        boost::property<boost::graph_graph_attribute_t, GraphvizAttributes, _GraphVertexAttribute>;
    using GraphvizGraphparameters =
        boost::property<boost::graph_name_t, std::string, _GraphGraphAttribute>;

    using GraphvizGraph =
        boost::adjacency_list<boost::setS,      /* Use std::set for out edges. */
                              boost::vecS,      /* Use std::vector for vertices. */
                              boost::directedS, /* Unidirectional connections between verts. */
                              GraphvizVertexparameters, /* Vertex Attributes. */
                              GraphvizEdgeparameters,   /* Edge attributes. */
                              GraphvizGraphparameters   /* Graph attributes. */
                              >;

    using VertexAttributePMap =
        boost::property_map<boost::subgraph<GraphvizGraph>, boost::vertex_attribute_t>::type;
    using EdgeAttributePMap =
        boost::property_map<boost::subgraph<GraphvizGraph>, boost::edge_attribute_t>::type;

public:
    GraphvizBuilder(typename NetworkT::SubGraph const& graph, std::string const& name)
      : gv_graph_(boost::num_vertices(graph))
    {
        boost::get_property(gv_graph_, boost::graph_name) = name;
        boost::get_property(gv_graph_, boost::graph_graph_attribute)["rankdir"] = "LR";

        for (typename NetworkT::SubGraph const& subg : boost::make_iterator_range(graph.children()))
        {
            // Property map accessors
            typename NetworkT::VertexBundleMapConst vertex_bundle =
                boost::get(boost::vertex_bundle, subg);

            typename NetworkT::InstanceType::Ptr instance =
                boost::get_property(subg, boost::graph_bundle).instance;

            boost::subgraph<GraphvizGraph>& gv_subg = gv_graph_.create_subgraph();
            boost::get_property(gv_subg, boost::graph_name) = "cluster_" + instance->get_name();
            boost::get_property(gv_subg, boost::graph_graph_attribute)["label"] =
                instance->get_import().get_model_name() + "::" + instance->get_name();
            boost::get_property(gv_subg, boost::graph_graph_attribute)["style"] = "filled";
            boost::get_property(gv_subg, boost::graph_vertex_attribute)["shape"] = "Mrecord";

            build_inport_vertices(subg, gv_subg, vertex_bundle, instance);
            build_outport_vertices(subg, gv_subg, vertex_bundle, instance);
            build_derivatives_vertices(subg, gv_subg, vertex_bundle, instance);
            build_states_vertices(subg, gv_subg, vertex_bundle, instance);
            build_parameters_vertices(subg, gv_subg, vertex_bundle, instance);
        }
        // Add a matching edge in the graphviz graph
        typename NetworkT::EdgeBundleMapConst edge_bundle = boost::get(boost::edge_bundle, graph);
        for (typename NetworkT::GraphEdge const& edge :
             boost::make_iterator_range(boost::edges(graph)))
        {
            std::pair<boost::subgraph<GraphvizGraph>::edge_descriptor, bool> gv_edge_pair =
                boost::add_edge(boost::source(edge, graph), boost::target(edge, graph), gv_graph_);

            EdgeAttributePMap edge_property_map = boost::get(boost::edge_attribute_t(), gv_graph_);

            if (edge_bundle[edge].flow)
            {
                // Edge is torn
                edge_property_map[gv_edge_pair.first]["color"] = "red";
            }

            switch (edge_bundle[edge].type)
            {
            case EdgeType::INTERNAL_INIT:
                edge_property_map[gv_edge_pair.first]["style"] = "dashed";
                edge_property_map[gv_edge_pair.first]["arrowhead"] = "empty";
                break;
            case EdgeType::INTERNAL_CTS:
                edge_property_map[gv_edge_pair.first]["style"] = "dashed";
                edge_property_map[gv_edge_pair.first]["arrowhead"] = "normal";
                break;
            case EdgeType::INTERNAL_BOTH:
                edge_property_map[gv_edge_pair.first]["style"] = "dashed";
                edge_property_map[gv_edge_pair.first]["arrowhead"] = "diamond";
                break;
            case EdgeType::CONNECTION:
                edge_property_map[gv_edge_pair.first]["style"] = "bold";
                break;
            case EdgeType::Count:
            default:
                throw std::runtime_error("Unexpected EdgeType.");
                break;
            }
        }
    }

    void write_to_file(boost::filesystem::path const& gv_path)
    {
        // Output to file
        boost::filesystem::ofstream gv_out(gv_path);
        boost::write_graphviz(gv_out, gv_graph_);
        BOOST_LOG_SEV(logger::get(), Severity::info) << "GraphViz written to " << gv_path;
    }

private:
    template <typename VertexBundleMap>
    void build_inport_vertices(typename NetworkT::SubGraph const& subg,
                               boost::subgraph<GraphvizGraph>& gv_subg,
                               VertexBundleMap& vertex_bundle,
                               typename NetworkT::InstanceType::Ptr instance)
    {
        /**** Handle INPORT vertices ****/
        typename NetworkT::GraphVertexPredicate inport_pred =
            [&](typename NetworkT::GraphVertex const& v) -> bool {
            return vertex_bundle[v].type == VertexType::INPORT;
        };
        boost::filtered_graph<typename NetworkT::SubGraph, boost::keep_all,
                              typename NetworkT::GraphVertexPredicate>
            in_vertex_subgraph(subg, boost::keep_all(), inport_pred);

        boost::subgraph<GraphvizGraph>& gv_subg_in = gv_subg.create_subgraph();
        boost::get_property(gv_subg_in, boost::graph_name) =
            "cluster_" + instance->get_name() + "_in";
        boost::get_property(gv_subg_in, boost::graph_graph_attribute)["label"] = "Inputs";

        for (auto vertex : boost::make_iterator_range(boost::vertices(in_vertex_subgraph)))
        {
            // Need to convert vertex into a global vertex
            typename NetworkT::GraphVertex global_vertex = subg.local_to_global(vertex);
            VertexAttributePMap vertex_property_map =
                boost::get(boost::vertex_attribute_t(), gv_subg_in);
            boost::subgraph<GraphvizGraph>::vertex_descriptor gv_vertex =
                boost::add_vertex(global_vertex, gv_subg_in);
            std::shared_ptr<typename NetworkT::VarType> variable = vertex_bundle[vertex].var;
            boost::get(vertex_property_map, gv_vertex)["label"] = variable->get_name();
            boost::get(vertex_property_map, gv_vertex)["color"] = "darkslateblue";
        }
    }

    template <typename VertexBundleMap>
    void build_outport_vertices(typename NetworkT::SubGraph const& subg,
                                boost::subgraph<GraphvizGraph>& gv_subg,
                                VertexBundleMap& vertex_bundle,
                                typename NetworkT::InstanceType::Ptr instance)
    {
        /**** Handle OUTPORT vertices ****/
        typename NetworkT::GraphVertexPredicate outport_pred =
            [&](typename NetworkT::GraphVertex const& v) -> bool {
            return vertex_bundle[v].type == VertexType::OUTPORT;
        };
        boost::filtered_graph<typename NetworkT::SubGraph, boost::keep_all,
                              typename NetworkT::GraphVertexPredicate>
            out_vertex_subgraph(subg, boost::keep_all(), outport_pred);

        boost::subgraph<GraphvizGraph>& gv_subg_out = gv_subg.create_subgraph();
        boost::get_property(gv_subg_out, boost::graph_name) =
            "cluster_" + instance->get_name() + "_out";
        boost::get_property(gv_subg_out, boost::graph_graph_attribute)["label"] = "Outputs";

        for (auto vertex : boost::make_iterator_range(boost::vertices(out_vertex_subgraph)))
        {
            // Need to convert vertex into a global vertex
            typename NetworkT::GraphVertex global_vertex = subg.local_to_global(vertex);
            VertexAttributePMap vertex_property_map =
                boost::get(boost::vertex_attribute_t(), gv_subg_out);
            boost::subgraph<GraphvizGraph>::vertex_descriptor gv_vertex =
                boost::add_vertex(global_vertex, gv_subg_out);
            std::shared_ptr<typename NetworkT::VarType> variable = vertex_bundle[vertex].var;
            boost::get(vertex_property_map, gv_vertex)["label"] = variable->get_name();
            boost::get(vertex_property_map, gv_vertex)["color"] = "darkorange";
        }
    }

    template <typename VertexBundleMap>
    void build_derivatives_vertices(typename NetworkT::SubGraph const& subg,
                                    boost::subgraph<GraphvizGraph>& gv_subg,
                                    VertexBundleMap& vertex_bundle,
                                    typename NetworkT::InstanceType::Ptr instance)
    {
        /**** Handle DER vertices ****/
        typename NetworkT::GraphVertexPredicate deriv_pred =
            [&](typename NetworkT::GraphVertex const& v) -> bool {
            return vertex_bundle[v].type == VertexType::DER;
        };
        boost::filtered_graph<typename NetworkT::SubGraph, boost::keep_all,
                              typename NetworkT::GraphVertexPredicate>
            der_vertex_subgraph(subg, boost::keep_all(), deriv_pred);

        boost::subgraph<GraphvizGraph>& gv_subg_der = gv_subg.create_subgraph();
        boost::get_property(gv_subg_der, boost::graph_name) =
            "cluster_" + instance->get_name() + "_der";
        boost::get_property(gv_subg_der, boost::graph_graph_attribute)["label"] = "Derivatives";

        for (auto vertex : boost::make_iterator_range(boost::vertices(der_vertex_subgraph)))
        {
            // Need to convert vertex into a global vertex
            typename NetworkT::GraphVertex global_vertex = subg.local_to_global(vertex);
            VertexAttributePMap vertex_property_map =
                boost::get(boost::vertex_attribute_t(), gv_subg_der);
            boost::subgraph<GraphvizGraph>::vertex_descriptor gv_vertex =
                boost::add_vertex(global_vertex, gv_subg_der);
            std::shared_ptr<typename NetworkT::VarType> variable = vertex_bundle[vertex].var;
            boost::get(vertex_property_map, gv_vertex)["label"] = variable->get_name();
            boost::get(vertex_property_map, gv_vertex)["color"] = "dodgerblue4";
        }
    }

    template <typename VertexBundleMap>
    void build_states_vertices(typename NetworkT::SubGraph const& subg,
                               boost::subgraph<GraphvizGraph>& gv_subg,
                               VertexBundleMap& vertex_bundle,
                               typename NetworkT::InstanceType::Ptr instance)
    {
        /**** Handle STATE vertices ****/
        typename NetworkT::GraphVertexPredicate states_pred =
            [&](typename NetworkT::GraphVertex const& v) -> bool {
            return vertex_bundle[v].type == VertexType::STATE;
        };
        boost::filtered_graph<typename NetworkT::SubGraph, boost::keep_all,
                              typename NetworkT::GraphVertexPredicate>
            state_vertex_subgraph(subg, boost::keep_all(), states_pred);

        boost::subgraph<GraphvizGraph>& gv_subg_state = gv_subg.create_subgraph();
        boost::get_property(gv_subg_state, boost::graph_name) =
            "cluster_" + instance->get_name() + "_state";
        boost::get_property(gv_subg_state, boost::graph_graph_attribute)["label"] = "States";

        for (auto vertex : boost::make_iterator_range(boost::vertices(state_vertex_subgraph)))
        {
            // Need to convert vertex into a global vertex
            typename NetworkT::GraphVertex global_vertex = subg.local_to_global(vertex);
            VertexAttributePMap vertex_property_map =
                boost::get(boost::vertex_attribute_t(), gv_subg_state);
            boost::subgraph<GraphvizGraph>::vertex_descriptor gv_vertex =
                boost::add_vertex(global_vertex, gv_subg_state);
            std::shared_ptr<typename NetworkT::VarType> variable = vertex_bundle[vertex].var;
            boost::get(vertex_property_map, gv_vertex)["label"] = variable->get_name();
            boost::get(vertex_property_map, gv_vertex)["color"] = "brown";
        }
    }

    template <typename VertexBundleMap>
    void build_parameters_vertices(typename NetworkT::SubGraph const& subg,
                                   boost::subgraph<GraphvizGraph>& gv_subg,
                                   VertexBundleMap& vertex_bundle,
                                   typename NetworkT::InstanceType::Ptr instance)
    {
        /**** Handle PARAM vertices ****/
        typename NetworkT::GraphVertexPredicate param_pred =
            [&](typename NetworkT::GraphVertex const& v) -> bool {
            return vertex_bundle[v].type == VertexType::PARAM;
        };
        boost::filtered_graph<typename NetworkT::SubGraph, boost::keep_all,
                              typename NetworkT::GraphVertexPredicate>
            param_vertex_subgraph(subg, boost::keep_all(), param_pred);

        boost::subgraph<GraphvizGraph>& gv_subg_param = gv_subg.create_subgraph();
        boost::get_property(gv_subg_param, boost::graph_name) =
            "cluster_" + instance->get_name() + "_param";
        boost::get_property(gv_subg_param, boost::graph_graph_attribute)["label"] = "Parameters";

        for (auto vertex : boost::make_iterator_range(boost::vertices(param_vertex_subgraph)))
        {
            // Need to convert vertex into a global vertex
            typename NetworkT::GraphVertex global_vertex = subg.local_to_global(vertex);
            VertexAttributePMap vertex_property_map =
                boost::get(boost::vertex_attribute_t(), gv_subg_param);
            boost::subgraph<GraphvizGraph>::vertex_descriptor gv_vertex =
                boost::add_vertex(global_vertex, gv_subg_param);
            std::shared_ptr<typename NetworkT::VarType> variable = vertex_bundle[vertex].var;
            boost::get(vertex_property_map, gv_vertex)["label"] = variable->get_name();
            boost::get(vertex_property_map, gv_vertex)["color"] = "green";
        }
    }
    boost::subgraph<GraphvizGraph> gv_graph_;
};

} /* namespace network */
} /* namespace boom */
} /* namespace quillum */
