/**
 *  @file   types.hpp
 *  @brief  Common types
 *  @since  March 15, 2017
 *  @author John Hughes <jondo2010@gmail.com>
 */
#pragma once

#include <boost/mpl/vector.hpp>
#include <boost/variant.hpp>

namespace quillum
{
namespace fmi
{
using FmiMappedTypes = boost::mpl::vector<double, int, bool, char*>;
using FmiVariant = boost::make_variant_over<FmiMappedTypes>::type;
} /* namespace fmi */
} /* namespace quillum */
