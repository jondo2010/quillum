/**
 *  @file   fmu_instance.hpp
 *  @brief
 *  @since  Nov 21, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */

#pragma once

#include <iterator>
#include <mutex>
#include <unordered_map>

#include <boost/exception/errinfo_api_function.hpp>
#include <boost/exception/errinfo_errno.hpp>
#include <boost/exception/errinfo_type_info_name.hpp>
#include <boost/functional/hash.hpp>

#include "fmu_import.hpp"
#include "logging.hpp"
#include "types.hpp"

namespace quillum
{
namespace fmi
{
template <class ImportT, class VariantT>
class FmuInstance
{
    /**
     * @brief   Overloaded ostream operator to write an FmuInstance to ostream
     */
    friend std::ostream &operator<<(std::ostream &, FmuInstance const &);

    using MutexType = std::mutex;
    using ReadLock = std::unique_lock<MutexType>;
    using WriteLock = std::unique_lock<MutexType>;

public:
    using ImportType = ImportT;
    using VariantType = VariantT;
    using VarType = typename ImportT::VarType;

    // Map a VarType to VariantType for parameters and initial conditions.
    using VarMap = std::unordered_map<VarType, VariantType, boost::hash<VarType>>;
    using Ptr = std::shared_ptr<FmuInstance>; /*!< Convenient pointer typedef. */

    /**
     * @brief   Struct containing event info from the FMU
     */
    struct EventInfo
    {
        bool new_discrete_states_needed;
        bool terminate;
        bool continuous_nominals_changed;
        bool continuous_states_changed;
        double next_event_time;
    };

    /**
     * @brief   Create a new FmuInstance
     * @param   import              A shared_ptr to the import
     * @param   name                The name of the instance
     * @param   visible             Visibility flag passed on to the FMU instance
     * @param   initial_conditions  A map of initial conditions to set before initialization.
     * @param   params              A map of parameter values to set during initialization.
     */
    FmuInstance(std::shared_ptr<ImportT const> import, std::string const &name, const bool visible,
                VarMap const &initial_conditions = VarMap(), VarMap const &params = VarMap());

    /**
     * Move constructor
     */
    FmuInstance(FmuInstance &&other);

    // FmuInstance& operator=(FmuInstance&& other);

    explicit FmuInstance(const FmuInstance &other) = default;

    ~FmuInstance();

    /**
     * @brief   Set debug logging on or off
     * @param   logging_on
     */
    void set_debug_logging(bool logging_on) const;

    /**
     * @brief Informs the FMU to setup the experiment.
     * @param tolerance     An optional tolerance value for the FMU. Will be ignored if <= 0.0.
     * @param start_time    The initial value of time for the FMU
     * @param stop_time     An optional stopping time for the FMU. Will be ignored if <= 0.0.
     */
    void setup_experiment(const double tolerance = 0.0, const double start_time = 0.0,
                          const double stop_time = -1.0) const;

    /**
     * @brief   Informs the FMU to enter Initialization Mode.
     */
    void enter_initialization() const;

    /**
     * @brief   Informs the FMU to exit Initialization Mode.
     */
    void exit_initialization() const;

    /**
     * @brief   Informs the FMU that the simulation run is terminated.
     */
    void terminate() const;

    /**
     * @brief   Is called by the environment to reset the FMU after a simulation run.
     */
    void reset() const;

    /**
     * @brief   Set the value of an FMU instance variable
     * @param   var     A Variable instance to set
     * @param   val     The value to set
     */
    void set_value(VarType const &var, VariantType const &val) const;

    /**
     * @brief   Get the value of an FMU instance variable
     * @param   v   A Variable instance to set
     * @return  The value
     */
    VariantType get_value(VarType const &var) const;

    /**
     * @brief   Get a the partial derivative of an unknown variable with respect to a known
     * variable.
     * @param[in]   unknown_var The unknown variable
     * @param[in]   known_var   The known variable
     * @return      The partial derivative
     */
    double get_directional_derivative(VarType const &unknown_var, VarType const &known_var) const;

    /*
    void Variable::set_derivative(const double val) const
    {
        const fmi2_integer_t orders[] = {1};
        fmi2_status_t stat = fmi2_import_set_real_input_derivatives(&import_, &vr_, 1, orders,
    &val);
        if (stat != fmi2_status_ok)
        {
            BOOST_LOG_SEV(logger::get(), Severity::error) << "Error in Variable::set_derivative(): "
                            << fmi2_import_get_last_error(&import_);
            throw ImportTException() << boost::errinfo_api_function("Variable::set_derivative()")
            << fmi_status(stat)
            << fmi_error_string(fmi2_import_get_last_error(&import_));
        }
    }
    */

    /**
     * @brief   The model enters Event Mode from the Continuous-Time Mode and discrete-time
     * equations may become active (and relations are not “frozen”).
     */
    void enter_event_mode() const;

    /**
     * @brief       The FMU is in Event Mode and the super dense time is incremented by this call.
     * @param[out]  event_info  Output event_info struct
     */
    EventInfo new_discrete_states() const;

    /**
     * @brief   The model enters Continuous-Time Mode and all discrete-time equations become
     * inactive and all relations are “frozen”.
     */
    void enter_continuous_time_mode() const;

    /**
     * @brief   Set a new time instant and re-initialize caching of variables that depend on time,
     * provided the newly provided time value is different to the previously set time value
     * (variables that depend solely on constants or parameters need not to be newly computed in the
     * sequel, but the previously computed values can be reused).
     * @param   time
     */
    void set_time(double time) const;

    /**
     * @brief   Set a new (continuous) state vector and re-initialize caching of variables that
     * depend on the states.
     * @param x
     */
    void set_continuous_states(const double *x) const;

    /**
     * @brief   Set a new (continuous) state vector and re-initialize caching of variables that
     * depend on the states.
     * @param x
     */
    void set_continuous_states(std::vector<double> const &x) const;

    /**
     * @brief       This function must be called by the environment after every completed step of
     *              the integrator provided the capability flag completedIntegratorStepNotNeeded =
     *              false.
     *
     * @param[in]   fix_prior_state     True if fmiSetFMUState will no longer be called for time
     *                                  instants prior to current time in this simulation run.
     * @param[out]  enter_event_mode    Call fmiEnterEventMode indicator.
     * @param[out]  terminate           Terminate simulation indicator.
     */
    void completed_integrator_step(bool fix_prior_state, bool &enter_event_mode,
                                   bool &terminate) const;

    /**
     * @brief       Fill a c-style array with the current state derivatives.
     * @param[out]  der A pointer to a contiguous array of allocated doubles to return the
     * derivatives into.
     */
    void get_derivatives(double *der) const;

    std::vector<double> get_derivatives() const;

    std::vector<double> get_event_indicators() const;

    /**
     * @brief   Get the current values of the continuous states
     */
    std::vector<double> get_continuous_states() const;

    /**
     * @brief   Get the name of this instance.
     */
    inline std::string const &get_name() const { return name_; }
    /**
     * @brief   Get a const reference to the ImportT for this FmuInstance
     */
    inline ImportT const &get_import() const { return *fmu_import_; }
    /**
     * @brief   Get the VarMap of initial condition values
     */
    inline VarMap const &get_initial_conditions() const { return initial_conditions_; }
    /**
     * @brief   Get the VarMap of initial parameter values
     */
    inline VarMap const &get_initial_params() const { return initial_params_; }
    // Hash support functions
    bool operator==(FmuInstance const &) const;
    friend std::size_t hash_value(FmuInstance const &);

private:
    FmuInstance(FmuInstance &&other, ReadLock rhs_lk);

    std::unique_ptr<LoggerType> log_source_; /*!< Logging source. */
    mutable MutexType mutex_; /*!< Mutex to ensure single-threaded access to the FMI API. */
    const std::string name_;  /*!< Local copy of instance name. */
    std::shared_ptr<ImportT const> fmu_import_; /*!< Reference to the ImportT. */
    std::function<void(fmi2_component_t)>
        component_deleter_; /*!< Memory freeing function for the fmi2_component_t. */
    std::unique_ptr<std::remove_pointer<fmi2_component_t>::type, decltype(component_deleter_)>
        component_;             /*!< Instance component handle. */
    VarMap initial_conditions_; /*!< Map of initial condition values. */
    VarMap initial_params_;     /*!< Map of initial parameter values. */
};

template <class ImportT, class VariantT>
inline std::ostream &operator<<(std::ostream &stream,
                                FmuInstance<ImportT, VariantT> const &instance)
{
    return stream << "<" << instance.get_import() << " instance '" << instance.get_name() << "'>";
}

template <class ImportT, class VariantT>
inline bool FmuInstance<ImportT, VariantT>::operator==(FmuInstance const &other) const
{
    return component_ == other.component_;
}

template <class ImportT, class VariantT>
inline std::size_t hash_value(FmuInstance<ImportT, VariantT> const &instance)
{
    boost::hash<size_t> hasher;
    return hasher(((size_t)instance.component_.get()) / sizeof(fmi2_component_t));
}

} /* namespace fmi */
} /* namespace quillum */
