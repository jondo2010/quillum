/**
 *  @file   fmu_instance.cpp
 *  @brief
 *  @since  Nov 21, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#include "fmu_instance.hpp"

#include <unordered_map>

#include <boost/bind.hpp>
#include <boost/format.hpp>

namespace quillum
{
namespace fmi
{
template <class ImportT, class VariantT>
FmuInstance<ImportT, VariantT>::FmuInstance(std::shared_ptr<ImportT const> import,
                                            std::string const &name, const bool visible,
                                            VarMap const &initial_conditions,
                                            VarMap const &initial_params)
  : log_source_(std::make_unique<LoggerType>(boost::log::keywords::channel = "Instance"))
  , name_(name)
  , fmu_import_(import)
  , component_deleter_(boost::bind(fmi2_import_free_instance, fmu_import_->import_.get(), _1))
  , component_(fmi2_import_instantiate(fmu_import_->import_.get(), name.c_str(),
                                       fmi2_model_exchange, NULL, fmi2_boolean_t(visible)),
               component_deleter_)
  , initial_conditions_(initial_conditions)
  , initial_params_(initial_params)
{
    BOOST_LOG_FUNCTION();
    log_source_->add_attribute("Module", boost::log::attributes::constant<std::string>(name_));

    BOOST_LOG_SEV(*log_source_, Severity::info)
        << boost::format("Instance created from [%1%].") % fmu_import_->get_model_name();

    if (component_ == NULL)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal) << "Error creating instance in FmuInstance()"
                                                     << fmu_import_->get_last_error();
        throw FmuImportException() << boost::errinfo_api_function("FmuInstance::FmuInstance()")
                                   << fmi_error_string(fmu_import_->get_last_error());
    }

    // Handle initial condition passed in
    for (typename VarMap::value_type const &p : initial_conditions)
    {
        if ((p.first.get_details().initial == fmi2_initial_enu_exact) ||
            (p.first.get_details().initial == fmi2_initial_enu_approx))
        {
            BOOST_LOG_SEV(*log_source_, Severity::info)
                << boost::format("Overriding start = %1% for [%2%] (from %3%)") % p.second %
                       p.first.get_name() % p.first.get_details().start;
            set_value(p.first, p.second);
        }
        else
        {
            BOOST_LOG_SEV(*log_source_, Severity::warning)
                << boost::format(
                       "Not overriding start value for [%1%], variable is not marked "
                       "initial=exact or initial=approx.") %
                       p.first.get_name();
        }
    }
}

template <class ImportT, class VariantT>
FmuInstance<ImportT, VariantT>::FmuInstance(FmuInstance &&other)
  : FmuInstance(std::forward<FmuInstance>(other), ReadLock(other.mutex_))
{
    BOOST_LOG_SEV(*log_source_, Severity::trace) << "FmuInstance(FmuInstance &&)";
}

template <class ImportT, class VariantT>
FmuInstance<ImportT, VariantT>::FmuInstance(FmuInstance &&other, ReadLock rhs_lk)
  : name_(std::move(other.name_))
  , fmu_import_(std::move(other.fmu_import_))
  , component_(std::move(other.component_))
{
    BOOST_LOG_SEV(*log_source_, Severity::trace) << "FmuInstance(FmuInstance &&, ReadLock)";
}

/*
FmuInstance& FmuInstance<ImportT>::operator=(FmuInstance&& other)
{
    if (this != &other)
    {
        WriteLock lhs_lk(mutex_, std::defer_lock);
        WriteLock rhs_lk(other.mutex_, std::defer_lock);
        std::lock(lhs_lk, rhs_lk);
        import_ = std::move(other.import_);
        instance_ = std::move(other.instance_);
    }
    return *this;
}
*/

template <class ImportT, class VariantT>
FmuInstance<ImportT, VariantT>::~FmuInstance()
{
    BOOST_LOG_SEV(*log_source_, Severity::trace) << "~FmuInstance()";
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::set_debug_logging(bool logging_on) const
{
    BOOST_LOG_FUNCTION();
    BOOST_LOG_SEV(*log_source_, Severity::trace) << "Setting debug logging: " << logging_on;

    WriteLock lock(mutex_);
    // TODO JH support categories
    const fmi2_status_t status = fmi2_import_set_debug_logging(
        fmu_import_->import_.get(), component_.get(), fmi2_boolean_t(logging_on), 0, NULL);

    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal) << "fmi2_import_set_debug_logging() failed in "
                                                        "set_debug_logging(): "
                                                     << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::set_debug_logging()") << fmi_status(status)
            << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::setup_experiment(const double tolerance,
                                                      const double start_time,
                                                      const double stop_time) const
{
    BOOST_LOG_FUNCTION();
    BOOST_LOG_SEV(*log_source_, Severity::trace)
        << boost::format("Setting up experiment from t=[%1%..%2%] with tol=[%3%].") % start_time %
               stop_time % tolerance;

    WriteLock lock(mutex_);
    const fmi2_status_t status = fmi2_import_setup_experiment(
        fmu_import_->import_.get(), component_.get(), fmi2_boolean_t(tolerance > 0.0), tolerance,
        start_time, fmi2_boolean_t(stop_time > 0.0), stop_time);
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal) << "fmi2_import_setup_experiment() failed in "
                                                        "setup_experiment(): "
                                                     << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::setup_experiment()") << fmi_status(status)
            << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::enter_initialization() const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status =
        fmi2_import_enter_initialization_mode(fmu_import_->import_.get(), component_.get());
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_enter_initialization_mode() failed in "
               "enter_initialization(): "
            << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::enter_initialization()")
            << fmi_status(status) << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::exit_initialization() const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status =
        fmi2_import_exit_initialization_mode(fmu_import_->import_.get(), component_.get());
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_exit_initialization_mode() failed in "
               "exit_initialization(): "
            << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::exit_initialization()")
            << fmi_status(status) << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::terminate() const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status =
        fmi2_import_terminate(fmu_import_->import_.get(), component_.get());
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_terminate() failed in terminate(): " << fmu_import_->get_last_error();
        throw FmuInstanceException() << boost::errinfo_api_function("FmuInstance::terminate()")
                                     << fmi_status(status)
                                     << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::reset() const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status = fmi2_import_reset(fmu_import_->import_.get(), component_.get());
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal) << "fmi2_import_reset() failed in reset(): "
                                                     << fmu_import_->get_last_error();
        throw FmuInstanceException() << boost::errinfo_api_function("FmuInstance::reset()")
                                     << fmi_status(status)
                                     << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::set_value(VarType const &var, VariantType const &val) const
{
    BOOST_LOG_FUNCTION();

    WriteLock lock(mutex_);
    const fmi2_value_reference_t vr[] = {var.get_value_reference()};
    fmi2_status_t stat;
    try
    {
        switch (var.get_details().type)
        {
        case fmi2_base_type_real:
            stat = fmi2_import_set_real(fmu_import_->import_.get(), component_.get(), vr, 1,
                                        &boost::get<double>(val));
            break;
        case fmi2_base_type_int:
            stat = fmi2_import_set_integer(fmu_import_->import_.get(), component_.get(), vr, 1,
                                           &boost::get<int>(val));
            break;
        case fmi2_base_type_bool:
            stat = fmi2_import_set_boolean(fmu_import_->import_.get(), component_.get(), vr, 1,
                                           (fmi2_boolean_t *)&boost::get<bool>(val));
            break;
        case fmi2_base_type_str:
            stat = fmi2_import_set_string(fmu_import_->import_.get(), component_.get(), vr, 1,
                                          (fmi2_string_t *)&boost::get<char *>(val));
            break;
        case fmi2_base_type_enum:
        default:
            BOOST_LOG_SEV(*log_source_, Severity::error) << "Unsupported FMI type in set_value()";
            throw FmuInstanceException()
                << boost::errinfo_api_function("Unsupported FMI type in FmuInstance::set_value()")
                << fmi_status(stat);
            break;
        }
    }
    catch (boost::bad_get const &e)
    {
        BOOST_LOG_SEV(*log_source_, Severity::error) << "set_value() called on an unmatched type.";
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::set_value() called on an unmatched type.");
    }

    if (stat != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::error) << "Error in set_value(): "
                                                     << fmu_import_->get_last_error();
        throw FmuInstanceException() << boost::errinfo_api_function("FmuInstance::set_value()")
                                     << fmi_status(stat)
                                     << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
VariantT FmuInstance<ImportT, VariantT>::get_value(VarType const &var) const
{
    BOOST_LOG_FUNCTION();

    WriteLock lock(mutex_);
    const fmi2_value_reference_t vr[] = {var.get_value_reference()};
    VariantType ret;
    fmi2_status_t stat;
    switch (var.get_details().type)
    {
    case fmi2_base_type_real:
    {
        fmi2_real_t val;
        stat = fmi2_import_get_real(fmu_import_->import_.get(), component_.get(), vr, 1, &val);
        ret = val;
    }
    break;
    case fmi2_base_type_int:
    {
        fmi2_integer_t val;
        stat = fmi2_import_get_integer(fmu_import_->import_.get(), component_.get(), vr, 1, &val);
        ret = val;
    }
    break;
    case fmi2_base_type_bool:
    {
        fmi2_boolean_t val;
        stat = fmi2_import_get_boolean(fmu_import_->import_.get(), component_.get(), vr, 1, &val);
        ret = (bool)val;
    }
    break;
    case fmi2_base_type_str:
    {
        fmi2_string_t val;
        stat = fmi2_import_get_string(fmu_import_->import_.get(), component_.get(), vr, 1, &val);
        ret = val;
    }
    break;
    case fmi2_base_type_enum:
    default:
        BOOST_LOG_SEV(*log_source_, Severity::error) << "Unsupported FMI type in get_value()";
        throw FmuInstanceException()
            << boost::errinfo_api_function("Unsupported FMI type in FmuInstance::get_value()")
            << fmi_status(stat);
        break;
    }

    if (stat != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::error) << "Error in get_value(): "
                                                     << fmu_import_->get_last_error();
        throw FmuInstanceException() << boost::errinfo_api_function("FmuInstance::get_value()")
                                     << fmi_status(stat)
                                     << fmi_error_string(fmu_import_->get_last_error());
    }
    return ret;
}

template <class ImportT, class VariantT>
double FmuInstance<ImportT, VariantT>::get_directional_derivative(VarType const &unknown_var,
                                                                  VarType const &known_var) const
{
    BOOST_LOG_FUNCTION();
    ReadLock lock(mutex_);

    // BOOST_LOG_SEV(log_source_, Severity::trace) << "Getting di(" << unknown_var << ")/di(" <<
    // known_var << ")";

    const fmi2_value_reference_t vr_unknown[] = {unknown_var.get_value_reference()};
    const fmi2_value_reference_t vr_known[] = {known_var.get_value_reference()};
    const fmi2_real_t dvKnown[] = {1.0};
    fmi2_real_t dvUnknown[] = {0.0};

    const fmi2_status_t status = fmi2_import_get_directional_derivative(
        fmu_import_->import_.get(), component_.get(), /* import, component */
        vr_known, 1U,                                 /* vKnown_ref[], nKnown */
        vr_unknown, 1U,                               /* vUnknown_ref[], nUnknown */
        dvKnown,                                      /* dvKnown[] */
        dvUnknown                                     /* dvUnknown[] */
        );

    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_get_directional_derivative() failed in "
               "get_directional_derivatives(): "
            << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::get_directional_derivatives()")
            << fmi_status(status) << fmi_error_string(fmu_import_->get_last_error());
    }
    return dvUnknown[0];
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::enter_event_mode() const
{
    WriteLock lock(mutex_);
    fmi2_status_t status =
        fmi2_import_enter_event_mode(fmu_import_->import_.get(), component_.get());
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal) << "fmi2_import_enter_event_mode() failed in "
                                                        "enter_event_mode(): "
                                                     << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::enter_event_mode()") << fmi_status(status)
            << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
typename FmuInstance<ImportT, VariantT>::EventInfo
FmuInstance<ImportT, VariantT>::new_discrete_states() const
{
    WriteLock lock(mutex_);
    fmi2_event_info_t fmi_event;
    const fmi2_status_t status =
        fmi2_import_new_discrete_states(fmu_import_->import_.get(), component_.get(), &fmi_event);
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_new_discrete_states() failed in "
               "new_discrete_states(): "
            << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::new_discrete_states()")
            << fmi_status(status) << fmi_error_string(fmu_import_->get_last_error());
    }

    return FmuInstance<ImportT, VariantT>::EventInfo(
        {(bool)fmi_event.newDiscreteStatesNeeded, (bool)fmi_event.terminateSimulation,
         (bool)fmi_event.nominalsOfContinuousStatesChanged,
         (bool)fmi_event.valuesOfContinuousStatesChanged, fmi_event.nextEventTime});
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::enter_continuous_time_mode() const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status =
        fmi2_import_enter_continuous_time_mode(fmu_import_->import_.get(), component_.get());
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_enter_continuous_time_mode() failed in "
               "enter_continuous_time_mode(): "
            << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::enter_continuous_time_mode()")
            << fmi_status(status) << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::set_time(const double time) const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status =
        fmi2_import_set_time(fmu_import_->import_.get(), component_.get(), time);
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_set_time() failed in set_time(): " << fmu_import_->get_last_error();
        throw FmuInstanceException() << boost::errinfo_api_function("FmuInstance::set_time()")
                                     << fmi_status(status)
                                     << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::set_continuous_states(const double *x) const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status = fmi2_import_set_continuous_states(
        fmu_import_->import_.get(), component_.get(), x, fmu_import_->number_of_continuous_states_);
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_set_continuous_states() failed in "
               "set_continuous_states(): "
            << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::set_continuous_states()")
            << fmi_status(status) << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::set_continuous_states(std::vector<double> const &x) const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status = fmi2_import_set_continuous_states(
        fmu_import_->import_.get(), component_.get(), &x[0], x.size());
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_set_continuous_states() failed in "
               "set_continuous_states(): "
            << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::set_continuous_states()")
            << fmi_status(status) << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::completed_integrator_step(bool fix_prior_state,
                                                               bool &enter_event_mode,
                                                               bool &terminate) const
{
    WriteLock lock(mutex_);
    const fmi2_status_t status = fmi2_import_completed_integrator_step(
        fmu_import_->import_.get(), component_.get(), fix_prior_state,
        (fmi2_boolean_t *)&enter_event_mode, (fmi2_boolean_t *)&terminate);
    if (status != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::fatal)
            << "fmi2_import_completed_integrator_step() failed in "
               "completed_integrator_step(): "
            << fmu_import_->get_last_error();
        throw FmuInstanceException()
            << boost::errinfo_api_function("FmuInstance::completed_integrator_step()")
            << fmi_status(status) << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
void FmuInstance<ImportT, VariantT>::get_derivatives(double *der) const
{
    WriteLock lock(mutex_);

    const fmi2_status_t stat =
        fmi2_import_get_derivatives(fmu_import_->import_.get(), component_.get(), der,
                                    fmu_import_->number_of_continuous_states_);
    if (stat != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::error) << "Error in get_derivatives(): "
                                                     << fmu_import_->get_last_error();
        throw FmuImportException() << boost::errinfo_api_function("FmuInstance::get_derivatives()")
                                   << fmi_status(stat)
                                   << fmi_error_string(fmu_import_->get_last_error());
    }
}

template <class ImportT, class VariantT>
std::vector<double> FmuInstance<ImportT, VariantT>::get_derivatives() const
{
    WriteLock lock(mutex_);

    std::vector<double> dx(fmu_import_->number_of_continuous_states_);

    const fmi2_status_t stat =
        fmi2_import_get_derivatives(fmu_import_->import_.get(), component_.get(), &dx[0],
                                    fmu_import_->number_of_continuous_states_);

    if (stat != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::error) << "Error in get_derivatives(): "
                                                     << fmu_import_->get_last_error();
        throw FmuImportException() << boost::errinfo_api_function("FmuInstance::get_derivatives()")
                                   << fmi_status(stat)
                                   << fmi_error_string(fmu_import_->get_last_error());
    }
    return dx;
}

template <class ImportT, class VariantT>
std::vector<double> FmuInstance<ImportT, VariantT>::get_event_indicators() const
{
    WriteLock lock(mutex_);
    std::vector<double> ind(fmu_import_->number_of_event_indicators_);
    const fmi2_status_t stat =
        fmi2_import_get_event_indicators(fmu_import_->import_.get(), component_.get(), &ind[0],
                                         fmu_import_->number_of_event_indicators_);
    if (stat != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::error) << "Error in get_event_indicators(): "
                                                     << fmu_import_->get_last_error();
        throw FmuImportException()
            << boost::errinfo_api_function("FmuInstance::get_event_indicators()")
            << fmi_status(stat) << fmi_error_string(fmu_import_->get_last_error());
    }
    return ind;
}

template <class ImportT, class VariantT>
std::vector<double> FmuInstance<ImportT, VariantT>::get_continuous_states() const
{
    WriteLock lock(mutex_);
    std::vector<double> x(fmu_import_->number_of_continuous_states_);
    const fmi2_status_t stat =
        fmi2_import_get_continuous_states(fmu_import_->import_.get(), component_.get(), &x[0],
                                          fmu_import_->number_of_continuous_states_);
    if (stat != fmi2_status_ok)
    {
        BOOST_LOG_SEV(*log_source_, Severity::error) << "Error in get_continuous_states(): "
                                                     << fmu_import_->get_last_error();
        throw FmuImportException()
            << boost::errinfo_api_function("FmuInstance::get_continuous_states()")
            << fmi_status(stat) << fmi_error_string(fmu_import_->get_last_error());
    }
    return x;
}

// Explicit Instantiations.
using Import = quillum::fmi::FmuImport;
using Variant = quillum::fmi::FmiVariant;
template FmuInstance<Import, Variant>::FmuInstance(std::shared_ptr<Import const>,
                                                   std::string const &, const bool, VarMap const &,
                                                   VarMap const &);
template FmuInstance<Import, Variant>::~FmuInstance();
template void FmuInstance<Import, Variant>::set_debug_logging(bool) const;
template void FmuInstance<Import, Variant>::setup_experiment(const double, const double,
                                                             const double) const;
template void FmuInstance<Import, Variant>::enter_initialization() const;
template void FmuInstance<Import, Variant>::exit_initialization() const;
template void FmuInstance<Import, Variant>::enter_continuous_time_mode() const;
template void FmuInstance<Import, Variant>::set_time(const double) const;
template void FmuInstance<Import, Variant>::completed_integrator_step(bool, bool &, bool &) const;
template std::vector<double> FmuInstance<Import, Variant>::get_derivatives() const;
template void FmuInstance<Import, Variant>::terminate() const;
template void FmuInstance<Import, Variant>::reset() const;
template void FmuInstance<Import, Variant>::set_value(VarType const &, VariantType const &) const;
template Variant FmuInstance<Import, Variant>::get_value(VarType const &) const;
template double FmuInstance<Import, Variant>::get_directional_derivative(VarType const &,
                                                                         VarType const &) const;

} /* namespace fmi */
} /* namespace quillum */
