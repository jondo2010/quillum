/**
 *  @file   fmu_import_variable.hpp
 *  @brief
 *  @since  Nov 16, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#pragma once

#include <memory>
#include <string>
#include <vector>

#include <boost/core/noncopyable.hpp>
#include <boost/exception/exception.hpp>
#include <boost/iterator/iterator_concepts.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/range/concepts.hpp>
#include <boost/range/iterator_range.hpp>

#include <fmilib.h>

namespace quillum
{
namespace fmi
{
struct FmuImportException : virtual boost::exception
{
};
struct FmuInstanceException : virtual boost::exception
{
};

typedef boost::error_info<struct tag_fmi_status, fmi2_status_t>
    fmi_status; /*!< error_info type to transport an fmi_status in an fmi exception. */
typedef boost::error_info<struct tag_fmi_error, char const*>
    fmi_error_string; /*!< error_info type to transport the error string in an fmi exception. */

class VariableIterator;

/**
 * @brief   Filter the variable on causality
 * @param   v       The variable to filter
 * @param   data    A void pointer to the causality value.
 * @return          1 if the variable causality matched, 0 otherwise.
 */
int causality_filter(const fmi2_import_variable_t* v, const void* data);

/**
 * @brief   Filter variables for having an exact or approximat initial value (indicating
 * a constant)
 * @param   v       The variable to filter
 * @param   data    A void pointer
 * @return          1 if the variable causality matched, 0 otherwise.
 */
int constant_filter(const fmi2_import_variable_t* v, const void* data);

std::ostream& operator<<(std::ostream&, fmi2_base_type_enu_t const&);

std::ostream& operator<<(std::ostream&, fmi2_causality_enu_t const&);

std::ostream& operator<<(std::ostream&, fmi2_variability_enu_t const&);

std::ostream& operator<<(std::ostream&, fmi2_initial_enu_t const&);

/**
 * @brief   Variable instances are wrapper objects around a variable defined in the FMU.
 */
class Variable
{
    /**
     * @brief   Overloaded ostream operator to write a Variable to ostream
     */
    friend std::ostream& operator<<(std::ostream&, Variable const&);

public:
    struct Details
    {
        Details(fmi2_import_variable_t* const v);

        const fmi2_base_type_enu_t type;          /*!< Variable base type. */
        const fmi2_causality_enu_t causality;     /*!< Variable causality. */
        const fmi2_variability_enu_t variability; /*!< Variability attribute. */
        const fmi2_initial_enu_t initial;         /*!< Variable initial type. */
        bool reinit;    /*!< If the real variable may change value at events. */
        double start;   /*!< Start value for the variable */
        double min;     /*!< Minimal value for the variable. */
        double max;     /*!< Maximum value for the variable. */
        double nominal; /*!< Nominal value for the variable. */
        std::string unit;
        std::string display_unit;
    };

    /**
     * @brief       	Construct a new Variable wrapper instance
     * @param v     	A pointer to the fmi2_import_variable_t to wrap
     * @param import	A const reference to the fmi2_import_t instance
     */
    Variable(fmi2_import_variable_t* const v, const std::shared_ptr<fmi2_import_t> import);

    /**
     * @brief   Dump a bunch of variable information to stdout.
     */
    void dump_info() const;

    /**
     * @brief   Get the name of the variable
     * @return  The name of the variable
     */
    const std::string get_name() const;

    /**
     * @brief   Get the variable description string
     * @return  Variable description or an empty std::string if there is none.
     * TODO Move this into Details
     */
    const std::string get_description() const;

    /**
     * @brief   Get the state variable corresponding to a derivative variable.
     * @return  A Variable of the state variable.
     */
    Variable get_state_for_derivative() const;

    inline fmi2_value_reference_t get_value_reference() const { return vr_; }
    inline Details const& get_details() const { return details_; }
    // Hash support functions
    bool operator==(Variable const&) const;
    bool operator!=(Variable const&) const;
    friend std::size_t hash_value(Variable const&);

private:
    inline fmi2_import_real_variable_t* as_real() const
    {
        return fmi2_import_get_variable_as_real(v_);
    }
    inline fmi2_import_integer_variable_t* as_integer() const
    {
        return fmi2_import_get_variable_as_integer(v_);
    }
    inline fmi2_import_bool_variable_t* as_bool() const
    {
        return fmi2_import_get_variable_as_boolean(v_);
    }
    inline fmi2_import_string_variable_t* as_string() const
    {
        return fmi2_import_get_variable_as_string(v_);
    }
    inline fmi2_import_enum_variable_t* as_enum() const
    {
        return fmi2_import_get_variable_as_enum(v_);
    }

    fmi2_import_variable_t* const v_;             /*!< Pointer to the variable. */
    const std::shared_ptr<fmi2_import_t> import_; /*!< Reference to imported fmu. */
    const fmi2_value_reference_t vr_;             /*!< Variable value reference. */
    const Details details_;                       /*!< Struct containing variable details. */
};

/**
 * @brief   VariableIterator is used to iterate through Variable objects in an FMU.
 */
class VariableIterator : public boost::iterator_facade<VariableIterator, Variable const,
                                                       boost::random_access_traversal_tag, Variable>
{
    friend class boost::iterator_core_access;

public:
    /**
     * @brief   Default construct an invalid iterator
     */
    VariableIterator()
      : vl_(0)
      , import_(0)
      , index_(0)
    {
    }

    explicit VariableIterator(const std::shared_ptr<fmi2_import_variable_list_t> vl,
                              const std::shared_ptr<fmi2_import_t> import, const size_t index)
      : vl_(vl)
      , import_(import)
      , index_(index)
    {
    }

private:
    const Variable dereference() const
    {
        return Variable(fmi2_import_get_variable(vl_.get(), index_), import_);
    }
    bool equal(VariableIterator const& other) const
    {
        return ((this->index_ == other.index_) && (this->vl_ == other.vl_));
    }
    void increment() { ++index_; }
    void decrement() { --index_; }
    void advance(const size_t n) { index_ += n; }
    difference_type distance_to(VariableIterator const& other) const
    {
        return other.index_ - index_;
    }

    std::shared_ptr<fmi2_import_variable_list_t> vl_; /**< Pointer to the variable list. */
    std::shared_ptr<fmi2_import_t> import_;           /*!< Reference to imported fmu. */
    std::size_t index_;                               /**< Current index into the variable list. */
};

BOOST_CONCEPT_ASSERT((boost_concepts::RandomAccessTraversal<VariableIterator>));

struct VariableRange
{
    typedef VariableIterator iterator;
    typedef VariableIterator const_iterator;
    typedef std::size_t size_type;
    VariableRange()
      : vl_(0)
      , import_(0)
      , len_(0)
    {
    }
    VariableRange(const std::shared_ptr<fmi2_import_variable_list_t> vl,
                  const std::shared_ptr<fmi2_import_t> import)
      : vl_(vl)
      , import_(import)
      , len_(fmi2_import_get_variable_list_size(vl.get()))
    {
    }
    const_iterator begin() const { return VariableIterator(vl_, import_, 0); }
    const_iterator end() const { return VariableIterator(vl_, import_, len_); }
    size_type size() const { return len_; }
private:
    std::shared_ptr<fmi2_import_variable_list_t> vl_; /**< Pointer to the variable list. */
    std::shared_ptr<fmi2_import_t> import_;           /*!< Reference to imported fmu. */
    size_t len_;                                      /**< Number of items in the variable list. */
};

BOOST_CONCEPT_ASSERT((boost::ForwardRangeConcept<VariableRange>));
BOOST_CONCEPT_ASSERT(
    (boost_concepts::ReadableIteratorConcept<typename boost::range_iterator<VariableRange>::type>));

} /* namespace fmi */
} /* namespace quillum */
