/**
 *  @file   config.hpp
 *  @brief  Main program configuration
 *  @since  Feb 7, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#include "config.hpp"

#include <boost/program_options.hpp>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

//#include "version.hpp"

namespace quillum
{
namespace boom
{
std::pair<ProgramConfig, bool> parse_options(int argc, char **argv)
{
    namespace po = boost::program_options;

    ProgramConfig config;
    bool want_exit = false;
    std::string raw_fmu_path;

    po::options_description cmdline_options;

    {
        po::options_description group("Generic Options");
        po::options_description_easy_init opts = group.add_options();
        opts("version,v", "Print version string");
        opts("help,h", "Produce help message");
        opts("fmu-path,L", po::value<std::string>(&raw_fmu_path)->default_value("."),
             "FMU search path, colon-separated");
        opts("net", po::value<boost::filesystem::path>(&config.network_file)->default_value(""),
             "Network configuration file to use");
        cmdline_options.add(group);
    }

    {
        po::options_description group("Logging Options");
        po::options_description_easy_init opts = group.add_options();
        opts("log-file,l", po::value<boost::filesystem::path>(&config.log_file)->default_value(""),
             "Output logging file");
        opts("min-log-level", po::value<quillum::Severity>(&config.severity)
                                  ->default_value(quillum::Severity::error)
                                  ->required(),
             std::string("Minimum logging level:\n\t[" + quillum::severityKeyList() + "]").c_str());

        opts("fmi-logging",
             po::value<bool>(&config.fmi_logging)->implicit_value(true)->default_value(false),
             "Enable logging from the FMI core");
        cmdline_options.add(group);
    }

    {
        po::options_description group("Debug Options");
        po::options_description_easy_init opts = group.add_options();
        opts("debug-network",
             po::value<bool>(&config.dump_and_quit)->implicit_value(true)->default_value(false),
             "Print debug information about each import in the network to stdout.");
        opts("graph-network",
             po::value<bool>(&config.graph_and_quit)->implicit_value(true)->default_value(false),
             "Write initial network data to a Graphviz file and quit");
        cmdline_options.add(group);
    }

    {
        po::options_description group("Simulation Options");
        po::options_description_easy_init opts = group.add_options();
        opts("trace-file,o",
             po::value<boost::filesystem::path>(&config.trace_file)->default_value(""),
             "Output trace file. Defaults to 'net_name.sdf'");
        opts("end-time", po::value<double>(&config.end_time)->default_value(2.0),
             "Simulaton end time.");
        opts("step-size", po::value<double>(&config.dt)->default_value(0.1), "Maximum step size");
        opts("tol", po::value<double>(&config.tolerance)->default_value(1.0e-6), "Error tolerance");
        cmdline_options.add(group);
    }

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(cmdline_options).run(), vm);
    po::notify(vm);

    // Split the raw_fmu_path into a vector of paths
    boost::split(config.fmu_paths, raw_fmu_path, boost::is_any_of(":"));

    if (vm.count("help"))
    {
        //std::cout << quillum::boom::build::identity::product_version() << '\n';
        std::cout << cmdline_options << std::endl;
        want_exit = true;
    }
    else if (vm.count("version"))
    {
        //std::cout << quillum::boom::build::identity::report() << std::endl;
        want_exit = true;
    }

    if (!boost::filesystem::exists(config.network_file))
    {
        if (config.network_file.empty())
        {
            std::cout << "No network configuration file specified!" << std::endl;
        }
        else
        {
            std::cout << "Network file not found: " << config.network_file << std::endl;
        }
        want_exit = true;
    }

    return std::make_pair(config, want_exit);
}

} /* namespace boom */
} /* namespace quillum */
