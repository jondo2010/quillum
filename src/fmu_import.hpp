/**
 *  @file   fmu_import.hpp
 *  @brief
 *  @since  Nov 16, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#pragma once

#include <memory>
#include <string>
#include <vector>

#include <boost/core/noncopyable.hpp>

#include "fmu_import_variable.hpp"
#include "logging.hpp"
#include "utility.hpp"

namespace quillum
{
namespace fmi
{
/**
 * @brief   FmuImport is used to import and manage a single FMU, and handles extraction, parsing,
 * and instantiation.
 * @tparam  VarIter The VariableIterator class to use.
 */
class FmuImport : private boost::noncopyable
{
    template <class ImportT, class VariantT>
    friend class FmuInstance;

    /**
     * @brief   Overloaded ostream operator to write an FmuImport to ostream
     */
    friend std::ostream& operator<<(std::ostream&, FmuImport const&);

public:
    using Ptr = std::shared_ptr<FmuImport>;           /*!< Convenient pointer typedef. */
    using VarRangeType = VariableRange;               /*!< Variable Range type. */
    using VarIterType = VarRangeType::const_iterator; /*!< Variable iterator type. */
    using VarType = VarIterType::value_type;          /*!< Variable type. */

    /**
     * @brief   Construct a new FmuImport
     * @param   fmu_path            The file path to the FMU to load
     * @param   enable_fmi_logging  Enable logging from the FMI core.
     */
    FmuImport(std::string const& fmu_path, const bool enable_fmi_logging = false);

    ~FmuImport();

    /**
     * @brief   Return a vector of the source files packaged in the FMU
     */
    std::vector<std::string> get_source_files() const;

    /**
     * @brief   Get the path to the shared library for this platform.
     */
    std::string get_shlib_path() const;

    /**
     * @brief   Get the temporary path to the extracted FMU.
     */
    std::string get_temp_path() const;

    /**
     * @brief   Get a Variable by name
     * @param   name    The name of the variable from the description XML
     * @return
     */
    VarType get_variable_by_name(std::string const& name) const;

    /**
     * @brief   Get an iterator_range of the input variables defined in the FMU
     * @return  An iterator_range spanning the input variables.
     */
    VarRangeType get_input_variable_range() const;

    /**
     * @brief   Get an iterator of filtered output variables defined in the FMU
     * @return  A VariableIterator pointing to the first Variable
     */
    VarIterType get_output_variable_iterator() const;

    /**
     * @brief   Get an iterator_range of the output variables defined in the FMU
     * @return  An iterator_range spanning the output variables.
     */
    VarRangeType get_output_variable_range() const;

    /**
     * @brief   Get an iterator of filtered variables defined in the FMU that are constants
     * and must be set before initialization.
     * @return  A VariableIterator pointing to the first Variable
     */
    VarRangeType get_constant_range() const;

    /**
     * @brief   Get an iterator_range of filtered variables defined in the FMU that are parameters.
     * @return  A VarRangeType of parameter variables.
     */
    VarRangeType get_parameter_range() const;

    /**
     * @brief   Get an iterator of filtered variables that are state derivatives
     * @return  A VariableIterator pointing to the first Variable
     */
    VarRangeType get_derivative_range() const;

    /**
     * @brief   Construct an iterator_range over input Variables that an output Variable depends on
     * @param	output	An output Variable to fetch dependencies for
     * @return  An iterator_range of input dependencies
     */
    VarRangeType get_output_dependency_range(VarType const& output) const;

    /**
     * @brief   Get an iterator_range of Variables that the state derivative depend upon.
     * @param   derivative  A derivative Variable to fetch dependencies for
     * @return  An iterator_range of dependencies
     */
    VarRangeType get_derivative_dependency_range(VarType const& derivative) const;

    /**
     * @brief   Get an iterator_range of Variables that the state derivatives depend upon.
     * @return  A iterator_range spanning the dependent Variables
     */
    VarRangeType get_derivative_dependency_range() const;

    /**
     * @brief   Get an iterator of Variables that are specified as initial unknowns.
     */
    VarRangeType get_initial_unknowns_range() const;

    /**
     * @brief   Get an iterator of Variables that an initial unknown Variable depends upon.
     */
    VarRangeType get_initial_unknowns_dependency_range(VarType const& initial_unknown) const;

    /**
     * @brief   Get the last reported error string
     * @return  Last reported error
     */
    const char* get_last_error() const;

    /**
     * @brief	Print the structure of this FMU to stdout
     */
    void print_structure() const;

    inline std::string const& get_model_name() const { return model_name_; }
    inline const char* get_description() const
    {
        return fmi2_import_get_description(import_.get());
    }

    inline size_t get_number_of_continuous_states() const { return number_of_continuous_states_; }
    inline size_t get_number_of_event_indicators() const { return number_of_event_indicators_; }
    inline LoggerType& get_log() { return *log_source_; }
private:
    /**
     * @brief   Get an iterator_range of dependent Variables corresponding to the given index and
     * the
     * row-compressed dependency data.
     * @param   start_index
     * @param   dependency
     * @param   factor_kind
     * @param   variable_index  The index into the dependency arrays as returned by
     * find_variable_index_in_iterator()
     */
    VarRangeType get_range_from_dependency_arrays(size_t* const& start_index,
                                                  size_t* const& dependency,
                                                  char* const& factor_kind,
                                                  const size_t variable_index) const;

    std::unique_ptr<LoggerType> log_source_; /*!< Logging source. */
    jm_callbacks jm_callbacks_;              /*!< Loader callback function structure. */
    std::unique_ptr<fmi_import_context_t, decltype(fmi_import_free_context)*> context_;
    TempDir temp_dir_;                      /*!< Temporary directory where FMU is unzipped. */
    const fmi_version_enu_t version_;       /*!< The reported FMU version. */
    std::shared_ptr<fmi2_import_t> import_; /*!< Pointer to the imported FMU. */
    const fmi2_callback_functions_t fmi2_callbacks_; /*!< FMI2.0 callback function structure. */
    const std::string model_name_;                   /*!< The reported model name. */
    size_t number_of_continuous_states_;             /*!< Number of reported continuous states. */
    size_t number_of_event_indicators_;              /*!< Number of reported event indicators. */
};
} /* namespace fmi */
} /* namespace quillum */
