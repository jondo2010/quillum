/**
 *  @file   config.hpp
 *  @brief  Main program configuration
 *  @since  Feb 7, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#pragma once

#include <string>

#include <boost/filesystem/path.hpp>

#include "logging.hpp"

namespace quillum
{
namespace boom
{
struct ProgramConfig
{
    std::vector<boost::filesystem::path> fmu_paths; /*!< A vector of paths to search for FMUs. */
    boost::filesystem::path network_file;           /*!< The path to the network definiton. */
    boost::filesystem::path trace_file;             /*!< The path to the output tracefile. */
    boost::filesystem::path log_file;               /*!< The path to the output log file. */
    quillum::Severity severity;
    double end_time;
    double dt;
    double tolerance;
    bool fmi_logging;
    bool dump_and_quit;
    bool graph_and_quit;
};

std::pair<ProgramConfig, bool> parse_options(int argc, char **argv);

} /* namespace boom */
} /* namespace quillum */
