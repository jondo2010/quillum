/**
 *  @file   logging.cpp
 *  @brief  Definitions for logging
 *  @since  Feb 10, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */

#include "logging.hpp"

#include <boost/assign.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/expressions/formatters/date_time.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>

#include <rang.hpp>

namespace quillum
{
// Initialize the global logging source
BOOST_LOG_GLOBAL_LOGGER_INIT(logger, LoggerType)
{
    LoggerType log;
    return log;
}

/**
 * @brief   Get a comma seperated string of the logging severity enum
 * @return  string of enum values
 */
const std::string severityKeyList()
{
    using namespace boost::assign;
    using namespace boost::adaptors;

    std::stringstream s;
    boost::copy(severity_map | map_values, std::ostream_iterator<std::string>(s, ","));
    return s.str();
}

void init_logging(const Severity level)
{
    namespace sinks = boost::log::sinks;
    namespace expr = boost::log::expressions;
    namespace attrs = boost::log::attributes;

    typedef sinks::synchronous_sink<sinks::text_ostream_backend> TextSink;
    boost::shared_ptr<TextSink> sink = boost::make_shared<TextSink>();
    boost::shared_ptr<std::ostream> stream(&std::cout, boost::null_deleter());

    sink->locked_backend()->add_stream(stream);
    sink->locked_backend()->auto_flush(true);

    sink->set_formatter(
        expr::stream
        << '[' << SeverityKeyword << "]\t"
        /*
        << "["
        << expressions::format_date_time<boost::posix_time::ptime>(
                      "TimeStamp",
                      "%Y-%m-%d %H:%M:%S"
                      )
        << "] "
        */
        << '[' << expr::attr<std::string>("Channel")
        << expr::if_(expr::has_attr("Module"))[expr::stream << '/' << rang::style::italic
                                                            << ModuleKeyword << rang::style::reset]
        << "] "
        /*
        << expr::if_(expr::has_attr(
                      "Scope"))[expr::stream << '('
                                             << expr::format_named_scope(
                                                    "Scope", boost::log::keywords::format = "[%n]",
                                                    boost::log::keywords::iteration = expr::forward,
                                                    boost::log::keywords::depth = 2)
                                             << ')']
                                             */
        << rang::style::bold << expr::smessage << rang::style::reset);
    boost::log::core::get()->add_sink(sink);
    boost::log::core::get()->set_filter(SeverityKeyword >= level);
    boost::log::add_common_attributes();
    boost::log::core::get()->add_global_attribute("Scope", attrs::named_scope());
}

std::istream& operator>>(std::istream& in, Severity& level)
{
    std::string token;
    in >> token;
    for (SeverityMap::const_iterator it = severity_map.begin(); it != severity_map.end(); ++it)
    {
        if (it->second == token)
        {
            level = it->first;
            return in;
        }
    }
    /// Didn't find the token, so thrown a validation_error
    throw boost::program_options::validation_error(
        boost::program_options::validation_error::invalid_option_value, "log-level", "token");
}

// The operator is used for regular stream formatting
std::ostream& operator<<(std::ostream& strm, const Severity level)
{
    switch (level)
    {
    case Severity::trace:
        strm << rang::fg::cyan;
        break;
    case Severity::debug:
        strm << rang::fg::blue;
        break;
    case Severity::info:
        strm << rang::fg::gray;
        break;
    case Severity::warning:
        strm << rang::fg::magenta;
        break;
    case Severity::error:
        strm << rang::fg::red;
        break;
    case Severity::fatal:
        strm << rang::fg::red;
        break;
    default:
        break;
    }
    strm << rang::style::bold << severity_map.at(level) << rang::style::reset;
    // strm << (char)std::toupper(severity_map.at(level)[0]);
    return strm;
}
} /* namespace quillum */
