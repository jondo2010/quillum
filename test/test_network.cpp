/**
 *  @file   test_network.cpp
 *  @brief
 *  @since  Nov 27, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */
#include <fstream>
#include <vector>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <boost/range/iterator_range.hpp>
#include <boost/range/size.hpp>

/*
#include <boost/archive/xml_oarchive.hpp>
#include <boost/graph/adj_list_serialize.hpp>
#include <boost/graph/graph_utility.hpp>
*/

#include "network.hpp"
#include "network_file.hpp"

#include "fmu_import.hpp"
#include "fmu_instance.hpp"
#include "test_mocks.hpp"

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

static const std::string test_data_path = STRINGIZE_VALUE_OF(TEST_DATA_PATH);

struct NetworkTest : public testing::Test
{
    /**
     * @brief   Called before each test is run
     */
    virtual void SetUp() {}
    /**
     * @brief   Called after each test is run.
     */
    virtual void TearDown() {}
    // clang-format off
    test::FakeFmuImport::Ptr prepare_fake_import()
    {
        test::FakeFmuImport::Ptr import = std::make_shared<test::FakeFmuImport>("fake_path", false);
        EXPECT_CALL(*import, get_variable_by_name("in_var1")) .WillRepeatedly(testing::Return(input_variables1[0]));
        EXPECT_CALL(*import, get_variable_by_name("in_var2")) .WillRepeatedly(testing::Return(input_variables1[1]));
        EXPECT_CALL(*import, get_variable_by_name("in_var3")) .WillRepeatedly(testing::Return(input_variables1[2]));
        EXPECT_CALL(*import, get_variable_by_name("out_var4")) .WillRepeatedly(testing::Return(output_variables1[0]));
        EXPECT_CALL(*import, get_variable_by_name("out_var5")) .WillRepeatedly(testing::Return(output_variables1[1]));

        EXPECT_CALL(*import, get_model_name()).WillRepeatedly(testing::Return("fake_fmu_import"));
        EXPECT_CALL(*import, get_input_variable_range()) .WillRepeatedly(testing::Return(boost::make_iterator_range(input_variables1)));
        EXPECT_CALL(*import, get_output_variable_range()) .WillRepeatedly(testing::Return(boost::make_iterator_range(output_variables1)));
        EXPECT_CALL(*import, get_output_dependency_range(output_variables1[0])) .WillRepeatedly(testing::Return(boost::make_iterator_range(depends_variables1)));
        EXPECT_CALL(*import, get_output_dependency_range(output_variables1[1])) .WillRepeatedly(testing::Return(boost::make_iterator_range(depends_variables2)));
        EXPECT_CALL(*import, get_number_of_continuous_states()).WillRepeatedly(testing::Return(2));
        EXPECT_CALL(*import, get_number_of_event_indicators()).WillRepeatedly(testing::Return(0));
        EXPECT_CALL(*import, get_derivative_dependency_range(testing::_)) .WillRepeatedly(testing::Return(boost::make_iterator_range(depends_variables3)));
        return import;
    }

    test::FakeFmuImport::Ptr prepare_direct_inertia_import()
    {
        test::FakeFmuImport::Ptr import = std::make_shared<test::FakeFmuImport>("fake_path", false);
        EXPECT_CALL(*import, get_variable_by_name("tauDrive")).WillRepeatedly(testing::Return(direct_inertial_inputs[0]));
        EXPECT_CALL(*import, get_variable_by_name("tau")).WillRepeatedly(testing::Return(direct_inertial_inputs[1]));
        EXPECT_CALL(*import, get_variable_by_name("phi")).WillRepeatedly(testing::Return(direct_inertial_outputs[0]));
        EXPECT_CALL(*import, get_variable_by_name("w")).WillRepeatedly(testing::Return(direct_inertial_outputs[1]));
        EXPECT_CALL(*import, get_variable_by_name("a")).WillRepeatedly(testing::Return(direct_inertial_outputs[2]));
        EXPECT_CALL(*import, get_model_name()).WillRepeatedly(testing::Return("Fake.DirectInertia"));
        EXPECT_CALL(*import, get_input_variable_range()).WillRepeatedly(testing::Return(boost::make_iterator_range(direct_inertial_inputs)));
        EXPECT_CALL(*import, get_output_variable_range()).WillRepeatedly(testing::Return(boost::make_iterator_range(direct_inertial_outputs)));
        EXPECT_CALL(*import, get_output_dependency_range(output_variables1[0])).WillRepeatedly(testing::Return(boost::make_iterator_range(depends_variables1)));
        EXPECT_CALL(*import, get_output_dependency_range(output_variables1[1])).WillRepeatedly(testing::Return(boost::make_iterator_range(depends_variables2)));
        return import;
    }

    std::vector<test::FakeVariable> direct_inertial_inputs
    {
        {.name_ = "tauDrive", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}},
        {.name_ = "tau", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}},
    };
    std::vector<test::FakeVariable> direct_inertial_outputs
    {
        {.name_ = "phi", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_output}},
        {.name_ = "w", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_output}},
        {.name_ = "a", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_output}}
    };

    std::vector<test::FakeVariable> input_variables1
    {
        {.name_ = "in_var1", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}},
        {.name_ = "in_var2", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}},
        {.name_ = "in_var3", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}}
    };
    std::vector<test::FakeVariable> output_variables1
    {
        {.name_ = "out_var4", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_output}},
        {.name_ = "out_var5", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_output}}
    };
    std::vector<test::FakeVariable> depends_variables1
    {
        {.name_ = "in_var1", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}},
        {.name_ = "in_var2", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}},
    };
    std::vector<test::FakeVariable> depends_variables2
    {
        {.name_ = "in_var3", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}},
    };
    std::vector<test::FakeVariable> depends_variables3
    {
        {.name_ = "in_var2", .details_ = {.type = fmi2_base_type_real, .causality = fmi2_causality_enu_input}},
    };
    // clang-format on
};

TEST(NetworkTestFreeFunctions, identify_connected_components) {}
/*
TEST_F(NetworkTest, add_import)
{
    quillum::fmi::network::Network<FakeFmuImport, FakeFmuInstance> net;
    FakeFmuImport::Ptr import = net.add_import("fake_path", false);
    ASSERT_TRUE(import);
    // 2nd call to add_import() should return the same import
    FakeFmuImport::Ptr import2 = net.add_import("fake_path", false);
    ASSERT_EQ(import, import2);
}
*/

TEST_F(NetworkTest, initialize_fmu)
{
    using Variant = quillum::fmi::FmiVariant;
    using Import = quillum::fmi::FmuImport;
    using Instance = quillum::fmi::FmuInstance<Import, Variant>;
    using Network = quillum::boom::network::Network<Instance>;

    quillum::boom::ProgramConfig config;
    config.network_file = "test.json";

    Import::Ptr import = std::make_shared<Import>(
        test_data_path + "/VehicleDynamics_Vehicles_Chassis_SingleTrack_Linear.fmu", true);
    Instance::Ptr instance = std::make_shared<Instance>(import, "test", false);

    Network net;
    quillum::boom::network::initialize_instance(net, instance, 0.0, true, config);

    quillum::boom::network::write_graphviz(net, config);
}

TEST_F(NetworkTest, initialize_instance)
{
    using ::testing::NiceMock;
    using ::testing::Return;

    quillum::boom::ProgramConfig config;
    quillum::boom::network::Network<test::FakeFmuInstance> net;
    test::FakeFmuImport::Ptr import = prepare_fake_import();

    {
        auto children = net.graph.children();
        EXPECT_EQ(0, std::distance(children.first, children.second));
    }

    test::FakeFmuInstance::VarMap ics;
    test::FakeFmuInstance::VarMap params;

    test::FakeFmuInstance::Ptr instance1 =
        quillum::boom::network::instance_factory(net, import, "fake_instance1", false, ics, params);
    ASSERT_TRUE(instance1);
    EXPECT_CALL(*instance1, set_debug_logging(false)).WillRepeatedly(Return());
    EXPECT_CALL(*instance1, setup_experiment(config.tolerance, 0.0, -1.0)).WillRepeatedly(Return());
    //EXPECT_CALL(*instance1, 
    quillum::boom::network::initialize_instance(net, instance1, 0.0, false, config);

    EXPECT_EQ(boost::num_vertices(net.graph), 7);
    EXPECT_EQ(boost::num_edges(net.graph), 6);

    auto children = net.graph.children();
    EXPECT_EQ(std::distance(children.first, children.second), 1);
    EXPECT_EQ(boost::num_vertices(*children.first), 7);
    EXPECT_EQ(boost::get_property(*children.first, boost::graph_bundle).instance, instance1);

    /*
    std::ofstream ofs("test.xml");
    boost::archive::xml_oarchive oa(ofs);
    oa << BOOST_SERIALIZATION_NVP(net.get_base_graph());
    */
}

TEST_F(NetworkTest, load_config_file)
{
    using namespace quillum::boom;
    ProgramConfig config;
    network::Network<test::FakeFmuInstance> net;

    network::ImportFactory<test::FakeFmuImport> import_factory = [](std::string const& name,
                                                                    const bool debug) {
        test::FakeFmuImport::Ptr import = std::make_shared<test::FakeFmuImport>("fake_path", false);
        return import;
    };

    network::InstanceFactory<test::FakeFmuInstance> instance_factory = [](
        test::FakeFmuImport::Ptr const& import, std::string const& name, const bool visible,
        test::FakeFmuInstance::VarMap const& initial_conditions,
        test::FakeFmuInstance::VarMap const& params) {
        test::FakeFmuInstance::Ptr instance = std::make_shared<test::FakeFmuInstance>(import, name, visible, initial_conditions, params);
        return instance;
    };

    quillum::boom::network::load_config_file(net, "simulator/tests/data/net1.json", config, import_factory, instance_factory);
}

/*
TEST_F(NetworkTest, tear_loops)
{
    quillum::boom::ProgramConfig config;
    config.network_file = "tear_loops.json";

    quillum::boom::network::Network<test::FakeFmuInstance> net;
    net.load_network_file("tear_loops.json", config);

    test::FakeFmuImport::Ptr import = prepare_fake_import();

    test::FakeFmuInstance::VarMap ics;
    test::FakeFmuInstance::VarMap params;

    ASSERT_TRUE(net.add_instance(import, "fake_instance1", false, ics, params));
    ASSERT_TRUE(net.add_instance(import, "fake_instance2", false, ics, params));
    ASSERT_TRUE(net.add_instance(import, "fake_instance3", false, ics, params));

    net.add_connection("fake_instance1", "out_var4", "fake_instance2", "in_var1");
    net.add_connection("fake_instance1", "out_var5", "fake_instance2", "in_var2");
    net.add_connection("fake_instance2", "out_var4", "fake_instance1", "in_var3");

    net.add_connection("fake_instance1", "out_var4", "fake_instance3", "in_var1");
    net.add_connection("fake_instance1", "out_var5", "fake_instance3", "in_var2");
    net.add_connection("fake_instance3", "out_var4", "fake_instance1", "in_var1");

    net.write_graphviz(config);
}
*/

int main(int argc, char** argv)
{
    quillum::init_logging(quillum::Severity::debug);
    quillum::logger::get().channel("UnitTest");

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

#include "network.cpp"
#include "network_file.cpp"
