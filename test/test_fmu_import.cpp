/**
 *  @file   test_fmu_import.cpp
 *  @brief
 *  @since  Nov 16, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */

#include <gtest/gtest.h>

#include "fmu_import.hpp"
#include "logging.hpp"

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

static const std::string test_data_path = STRINGIZE_VALUE_OF(TEST_DATA_PATH);

TEST(FmuImport, bad_create)
{
    ASSERT_THROW({ quillum::fmi::FmuImport import("", true); }, std::invalid_argument);
    ASSERT_THROW({ quillum::fmi::FmuImport import("non_existent.fmu", true); },
                 std::invalid_argument);
}

TEST(FmuImport, coupled_clutches)
{
    using Import = quillum::fmi::FmuImport;

    Import::Ptr import = std::make_shared<Import>(test_data_path + "/CoupledClutches.fmu", true);

    EXPECT_FALSE(import->get_temp_path().empty());
    EXPECT_FALSE(import->get_shlib_path().empty());
    EXPECT_TRUE(import->get_source_files().empty());

    EXPECT_EQ(import->get_model_name(), "CoupledClutches");

    Import::VarRangeType input_vars = import->get_input_variable_range();
    EXPECT_EQ(input_vars.size(), 1);

    Import::VarRangeType output_vars = import->get_output_variable_range();
    EXPECT_EQ(output_vars.size(), 4);

    Import::VarRangeType param_vars = import->get_parameter_range();
    EXPECT_EQ(param_vars.size(), 4);

    Import::VarRangeType der_vars = import->get_derivative_range();
    EXPECT_EQ(der_vars.size(), 18);

    EXPECT_THROW({ import->get_variable_by_name("non_existant"); }, std::runtime_error);

    Import::VarType output1 = import->get_variable_by_name("outputs[1]");
    Import::VarType output2 = import->get_variable_by_name("outputs[2]");
    Import::VarType output3 = import->get_variable_by_name("outputs[3]");
    Import::VarType output4 = import->get_variable_by_name("outputs[4]");
    Import::VarType input = import->get_variable_by_name("inputs");

    EXPECT_EQ(output1.get_name(), "outputs[1]");
    EXPECT_EQ(output2.get_name(), "outputs[2]");
    EXPECT_EQ(output3.get_name(), "outputs[3]");
    EXPECT_EQ(output4.get_name(), "outputs[4]");
    EXPECT_EQ(input.get_name(), "inputs");

    EXPECT_EQ(output1.get_details().type, fmi2_base_type_real);
    EXPECT_EQ(output2.get_details().type, fmi2_base_type_real);
    EXPECT_EQ(output3.get_details().type, fmi2_base_type_real);
    EXPECT_EQ(output4.get_details().type, fmi2_base_type_real);
    EXPECT_EQ(input.get_details().type, fmi2_base_type_real);

    EXPECT_EQ(*output_vars.begin(), output1);
}

TEST(FmuImport, single_track)
{
    using Import = quillum::fmi::FmuImport;

    Import::Ptr import = std::make_shared<Import>(
        test_data_path + "/VehicleDynamics_Vehicles_Chassis_SingleTrack_Linear.fmu", true);
    import->print_structure();

    EXPECT_EQ(import->get_model_name(), "VehicleDynamics.Vehicles.Chassis.SingleTrack.Linear");

    {
        Import::VarRangeType input_vars = import->get_input_variable_range();
        EXPECT_EQ(input_vars.size(), 2);
        EXPECT_EQ((input_vars.begin() + 0)->get_name(), "steering_angle");
        EXPECT_EQ((input_vars.begin() + 1)->get_name(), "longitudinal_velocity");
    }

    {
        Import::VarRangeType output_vars = import->get_output_variable_range();
        EXPECT_EQ(output_vars.size(), 2);
        EXPECT_EQ((output_vars.begin() + 0)->get_name(), "lateral_velocity");
        EXPECT_EQ((output_vars.begin() + 1)->get_name(), "yaw_rate");
    }

    {
        Import::VarRangeType vars = import->get_derivative_range();
        EXPECT_EQ(vars.size(), 2);
        EXPECT_EQ((vars.begin() + 0)->get_name(), "der(w_z)");
        EXPECT_EQ((vars.begin() + 1)->get_name(), "der(v_y)");

        Import::VarRangeType dep_var1 = import->get_derivative_dependency_range(*vars.begin());
        EXPECT_EQ(dep_var1.size(), 4);

        Import::VarRangeType dep_var2 = import->get_derivative_dependency_range(*(vars.begin()+1));
        EXPECT_EQ(dep_var2.size(), 4);
    }
}

int main(int argc, char** argv)
{
    quillum::init_logging(quillum::Severity::info);
    quillum::logger::get().channel("UnitTest");

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
