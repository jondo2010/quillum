/**
 *  @file   test_mocks.cpp
 *  @brief
 *  @since  May 3, 2017
 *  @author John Hughes <jondo2010@gmail.com>
 */

#include <gmock/gmock.h>

#include <fmilib.h>

#include "types.hpp"

namespace test
{
struct FakeVariable
{
    struct Details
    {
        fmi2_base_type_enu_t type;
        fmi2_causality_enu_t causality;
    };
    inline std::string get_name() const { return name_; }
    inline Details const& get_details() const { return details_; }
    bool operator==(FakeVariable const& other) const { return name_ == other.name_; }
    // MOCK_CONST_METHOD0(get_state_for_derivative, void());
    FakeVariable get_state_for_derivative() const { return FakeVariable(); }
    std::string name_;
    Details details_;
    friend std::ostream& operator<<(std::ostream&, FakeVariable const&);
};

struct FakeFmuImport
{
    typedef std::shared_ptr<FakeFmuImport> Ptr;
    typedef std::vector<FakeVariable>::iterator VarIterType;
    typedef FakeVariable VarType;
    FakeFmuImport(std::string s, bool)
    {
        ON_CALL(*this, get_model_name()).WillByDefault(testing::Return(s + "_fake_import"));
    };

    MOCK_CONST_METHOD1(get_variable_by_name, VarType(const std::string));
    MOCK_CONST_METHOD0(get_model_name, std::string());
    MOCK_CONST_METHOD0(get_input_variable_range, boost::iterator_range<VarIterType>());
    MOCK_CONST_METHOD0(get_output_variable_range, boost::iterator_range<VarIterType>());
    MOCK_CONST_METHOD0(get_derivative_range, boost::iterator_range<VarIterType>());
    MOCK_CONST_METHOD1(get_output_dependency_range,
                       boost::iterator_range<VarIterType>(VarType const&));
    MOCK_CONST_METHOD1(get_derivative_dependency_range,
                       boost::iterator_range<VarIterType>(VarType const&));
    MOCK_CONST_METHOD0(get_number_of_continuous_states, size_t());
    MOCK_CONST_METHOD0(get_number_of_event_indicators, size_t());
    MOCK_CONST_METHOD0(get_initial_unknowns_range, boost::iterator_range<VarIterType>());
    MOCK_CONST_METHOD1(get_initial_unknowns_dependency_range,
                       boost::iterator_range<VarIterType>(VarType const&));
};

struct FakeFmuInstance
{
    using ImportType = FakeFmuImport;
    using VariantType = quillum::fmi::FmiVariant;
    using VarType = FakeVariable;
    using VarMap = std::unordered_map<VarType, VariantType, boost::hash<VarType>>;

    typedef std::shared_ptr<FakeFmuInstance> Ptr;
    FakeFmuInstance(FakeFmuImport::Ptr import, const std::string name, const bool visible,
                    VarMap const& initial_conditions, VarMap const& params)
      : import_(import)
      , name_(name)
    {
        using ::testing::_;
        ON_CALL(*this, set_debug_logging(_)).WillByDefault(::testing::Return());
        ON_CALL(*this, setup_experiment(_,_,_)).WillByDefault(::testing::Return());
    }
    FakeFmuImport const& get_import() { return *import_; }
    inline std::string get_name() const { return name_; }
    MOCK_METHOD1(set_debug_logging, void(bool));
    MOCK_METHOD3(setup_experiment, void(const double, const double, const double));

    FakeFmuImport::Ptr import_;
    std::string name_;
};

struct FakeNetwork
{
    using VarType = FakeVariable;
    using VariantType = quillum::fmi::FmiVariant;
    using ImportType = FakeFmuImport;
    using InstanceType = FakeFmuInstance;
    using VarMap = std::unordered_map<VarType, VariantType, boost::hash<VarType>>;

    MOCK_CONST_METHOD0(get_config, quillum::boom::ProgramConfig());
};

inline std::size_t hash_value(FakeVariable const& var) { return boost::hash_value(var.name_); }
inline std::size_t hash_value(FakeFmuInstance const& instance)
{
    boost::hash<size_t> hasher;
    return hasher((size_t)instance.import_.get());
}

std::ostream& operator<<(std::ostream& stream, FakeVariable const& variable)
{
    return stream << variable.get_name() << " <"
                  //<< variable.get_details().type
                  << '/'
                  //<< variable.get_details().causality
                  << '>';
}
} /* namespace test */
