/**
 *  @file   test_fmu_import.cpp
 *  @brief
 *  @since  Nov 16, 2016
 *  @author John Hughes <jondo2010@gmail.com>
 */

#include <gtest/gtest.h>

#include "logging.hpp"
#include "utility.hpp"

TEST(Utility, find_file_in_path)
{
    std::vector<boost::filesystem::path> paths = {".", "/bin", "/usr/bin"};

    EXPECT_STREQ(quillum::find_file_in_path("ls", paths).c_str(), "/bin/ls");
    EXPECT_STREQ(quillum::find_file_in_path("python2.7", paths).c_str(), "/usr/bin/python2.7");
    EXPECT_STREQ(quillum::find_file_in_path("asdf1234", paths).c_str(), "asdf1234");
}

int main(int argc, char** argv)
{
    quillum::init_logging(quillum::Severity::info);
    quillum::logger::get().channel("UnitTest");

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
