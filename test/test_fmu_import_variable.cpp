/**
 *  @file   test_fmu_import_variable.cpp
 *  @brief
 *  @since  Nov 16, 2016
 *  @author johughes <jondo2010@gmail.com>
 */

#include <gtest/gtest.h>

#include "fmu_import.hpp"
#include "fmu_import_variable.hpp"
#include "logging.hpp"

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

static const std::string test_data_path = STRINGIZE_VALUE_OF(TEST_DATA_PATH);

TEST(FmuImportVariable, test_hash_and_equality)
{
    using Import = quillum::fmi::FmuImport;

    Import::Ptr import = std::make_shared<Import>(test_data_path + "/CoupledClutches.fmu", true);

    Import::VarType u1a = import->get_variable_by_name("inputs");
    Import::VarType u1b = import->get_variable_by_name("inputs");

    Import::VarType y1a = import->get_variable_by_name("outputs[1]");
    Import::VarType y1b = import->get_variable_by_name("outputs[1]");

    EXPECT_EQ(hash_value(u1a), hash_value(u1b));
    EXPECT_EQ(u1a, u1b);

    EXPECT_NE(hash_value(u1a), hash_value(y1a));
    EXPECT_NE(u1a, y1a);
}

int main(int argc, char **argv)
{
    quillum::init_logging(quillum::Severity::debug);
    quillum::logger::get().channel("UnitTest");

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
