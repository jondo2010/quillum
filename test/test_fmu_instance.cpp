/**
 *  @file   test_fmu_instance.cpp
 *  @brief
 *  @since  May 18, 2017
 *  @author John Hughes <jondo2010@gmail.com>
 */

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "fmu_instance.hpp"
#include "logging.hpp"

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

static const std::string test_data_path = STRINGIZE_VALUE_OF(TEST_DATA_PATH);

MATCHER(DoubleEqual, "")
{
    using namespace ::testing;
    return Matches(DoubleNear(std::get<1>(arg), 1e-5))(std::get<0>(arg));
}

TEST(FmuInstance, coupled_clutches)
{
    using Variant = quillum::fmi::FmiVariant;
    using Import = quillum::fmi::FmuImport;
    using Instance = quillum::fmi::FmuInstance<Import, Variant>;

    Import::Ptr import = std::make_shared<Import>(test_data_path + "/CoupledClutches.fmu", true);

    Import::VarType input = import->get_variable_by_name("inputs");
    Import::VarType T2 = import->get_variable_by_name("CoupledClutches1_T2");

    Instance::Ptr instance = std::make_shared<Instance>(import, "test", false);
    EXPECT_NO_THROW(instance->set_debug_logging(true));
    EXPECT_NO_THROW(instance->setup_experiment());
    EXPECT_NO_THROW(instance->enter_initialization());

    // This should throw since we're trying to set an integer into a real variable
    EXPECT_THROW({ instance->set_value(input, (int)5); }, quillum::fmi::FmuInstanceException);

    EXPECT_NO_THROW({ instance->set_value(input, (double)5.0); });
    EXPECT_EQ(boost::get<double>(instance->get_value(input)), (double)5.0);

    EXPECT_NO_THROW({ instance->set_value(T2, (double)5.0); });
    EXPECT_EQ(boost::get<double>(instance->get_value(T2)), (double)5.0);

    EXPECT_NO_THROW(instance->exit_initialization());
    EXPECT_NO_THROW(instance->enter_continuous_time_mode());
    EXPECT_NO_THROW(instance->set_time(123));

    const std::vector<double> expected_ders{10,        8.09485,   0,          -8.09485,   0,
                                            -50,       0,         50,         0.0616502,  0.0099995,
                                            0.0587304, 0.0560692, 0.00952428, 0.00909136, 0.999999,
                                            0.777875,  0.99995,   0.196494};

    EXPECT_THAT(instance->get_derivatives(), testing::Pointwise(DoubleEqual(), expected_ders));

    bool enter_event_mode;
    bool terminate;
    EXPECT_NO_THROW({ instance->completed_integrator_step(true, enter_event_mode, terminate); });
    EXPECT_EQ(enter_event_mode, false);
    EXPECT_EQ(terminate, false);

    EXPECT_NO_THROW({ instance->reset(); });
    // instance.terminate();
}

int main(int argc, char** argv)
{
    quillum::init_logging(quillum::Severity::info);
    quillum::logger::get().channel("UnitTest");

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
